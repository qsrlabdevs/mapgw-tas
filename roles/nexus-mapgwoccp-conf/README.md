Role Name
=========

It configures CamelService component on TAS. 

Requirements
------------

 * 

Role Variables
--------------

 * redis_url: specifies redis queuedb database
 * redis_queue: specifies egress_queue used by camel service for sending events
 * redis_pool_size: specifies redis pool size
 * svc_link: CamelService.ear
 * imscapp_file: specify vTAS imscapp file -  imscapp-CamelService.properties
 * icui_file: specify file that contains camel service announcements - CamelService.xml
 * CamelService requires tcap_provider, which should be set separately
    * ss7_stack: name of the SS7 Stack defined in USP-M, default: SS7_Stack_1
    * ss7_ssn: SSN used by TCAP provider for incoming InitialDPs - default: 146
    * ss7_pc : PointCode user by TCAP provider 
    * ss7_gt:  GT used by USP-M for SS7 Stack defined
    * ss7_app_id: specify application server instance id see-ID
    * instance_id: instance id is incremented for each see- instance defined

  
 
Dependencies
------------

 * none

Example Playbook
----------------


    - hosts: servers
      roles:
         - role: nexus-capgwoccp-conf
           redis_url: redis://redisip:26380/queuedb
           redis_pool_size: 10
           redis_queue: camel_events
           svc_link: CamelService.ear
           imscapp_file: imscapp-CamelService.properties
           icui_file: CamelService.xml
           ss7_stack: SS7_Stack_1
           ss7_ssn: 146
           ss7_pc : 10
           ss7_gt: 43700100200
           ss7_app_id: 1
           instance_id: 1
           template: tasgw-template.json.j2


License
-------

Quasar Software Research (c) 2018


