package io.qsrlabs.nexus.tas.cap.utils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerFactory;
import com.hp.opencall.ngin.timer.TimerListener;

public class TelecomUtils {
	private static final Logger _log = LoggerFactory.getLogger(TelecomUtils.class.getName());

	public static Long getCurrentTimeUnixFraction(){
		return (System.currentTimeMillis()%1000l);
	}

	public static Long getCurrentTimeUnixFormat(){
		return (System.currentTimeMillis()/1000l);
	}
	public  static String removePrefix(String number){
		if(number!=null && number.startsWith("+49")){
			return number.substring(3);
		}
		return number;
	}
	public static String getCurrentSystemDateTimeSeconds(){
		_log.debug("getCurrentSystemDateTime");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		_log.debug("formated data" + dateFormat.format(date));
		return dateFormat.format(date);
	}
	public static String getCurrentSystemDateTimeSecondsMiliSeconds(){
		_log.debug("getCurrentSystemDateTime");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = new Date();
		_log.debug("formated data" + dateFormat.format(date));
		return dateFormat.format(date);
	}
	public static String getSystemTimeZone(){
		_log.debug("getSystemTimeZone");
		DateFormat dateFormat = new SimpleDateFormat("z");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public static Timer createTimer(TimerListener timerListener, long delay,ApplicationSession appSession, TimerInformation.TIMER_TYPE timerType) {
		_log.debug("->createTimer() for {} with delay: {}",timerType,delay);

		TimerInformation timerInformation=new TimerInformation();
		timerInformation.setTimerType(timerType);
		return TimerFactory.instance().createTimer(appSession, timerListener, delay, timerInformation);
	}
	
	public static Map<String, Object> addressToJson(com.hp.opencall.ngin.scif.Address addr){
		_log.debug("in address:{}",addr);
		Map<String, Object> jsAddr = new HashMap<>();
		if( addr==null){
			return jsAddr;
		}
		if( addr.getDisplayName()!=null){
			jsAddr.put("displayName", addr.getDisplayName());
		}
		if( addr.getUser()!=null){
			jsAddr.put("user", addr.getUser());
		}
		if( addr.getScheme()!=null){
			jsAddr.put("scheme", addr.getScheme());
		}
		Map<String, String> params = new HashMap<>();
		jsAddr.put("parameters", params);
		for( Entry<String, String> param : addr.getParameters()){
			params.put(param.getKey(), param.getValue());
		}
		
		_log.debug("out JSON ADDR:{}",jsAddr);
		return jsAddr;
	}
	
	public static com.hp.opencall.ngin.scif.Address mapToAddr(Map<String, Object> jsAddr){
		_log.debug("in Address:{}",jsAddr);
		com.hp.opencall.ngin.scif.Address ret = new com.hp.opencall.ngin.scif.Address();
		ret.setScheme("tel");
		String valueStr = (String)jsAddr.get("scheme");
		if( valueStr!=null){
			ret.setScheme(valueStr);
		}
		valueStr = (String)jsAddr.get("user");
		if( valueStr!=null){
			ret.setUser(valueStr);
		}
		valueStr = (String)jsAddr.get("displayName");
		if( valueStr!=null){
			ret.setDisplayName(valueStr);
		}
		
		Map<String, String> params = (Map<String, String>) jsAddr.get("parameters");
		if( params!=null){
			for(Entry<String, String> key: params.entrySet()){
				ret.setParameter(key.getKey(), key.getValue());

			}
		}
		_log.debug("out Address:{}",ret.toString());
		
		return ret;
	}
	
}
