package io.qsrlabs.nexus.tas.utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.qsrlabs.nexus.tas.cap.businesslogic.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.netfunc.NetFuncException;
import com.hp.opencall.ngin.netfunc.SubsContextUser;
import com.hp.opencall.ngin.scif.parameters.map.RoutingInfoForShortMessageRes;
import com.hp.opencall.ngin.scif.parameters.map.SubscriberInfoResponse;
import com.hp.opencall.ngin.scif.parameters.map.SubscriberStaticInfoResponse;
import com.hp.opencall.ngin.scif.parameters.map.SubscriptionInformationModificationRes;

public class JSONSubsContextUser implements SubsContextUser {
	private static final Logger _log = LoggerFactory.getLogger(JSONSubsContextUser.class.getName());
	
	private Map<String, Object> atiReqJson;
	
	public JSONSubsContextUser(Map<String, Object> atiReqJson){
		this.atiReqJson = atiReqJson;
	}
	

	@Override
	public void reportIdentification(IdentificationData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportPortedNumberData(PortedNumberResData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportRoaming(RoamingData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportRountingInfoSMData(RoutingInfoForShortMessageRes arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportSubscriberContextDynamicInfo(SubscriberInfoResponse resp, NetFuncException arg1) {
		_log.debug("reportSubscriberContextDynamicInfo resp:{}",resp);
		
		//generate MAP for received subscriber location
		Map<String, Object> atiRespJson = new HashMap<>();
		atiRespJson.put("callid", atiReqJson.get("callid"));
		atiRespJson.put("queue", atiReqJson.get("queue"));
		atiRespJson.put("as", Parameters.get().getImsName());
		atiRespJson.put("eventtime", Calendar.getInstance().getTimeInMillis());
		
		Map<String, Object> ati = new HashMap<>();
		atiRespJson.put("atiResp", ati);
		
		//put subscriber state
		if(resp!=null){
			if( resp.getSubscriberInfo().getLocationInformation()!=null){
				Map<String, Object> locInfo = new HashMap<>();
				ati.put("locationInfomation", locInfo);
//				if( resp.getSubscriberInfo().getLocationInformation().)
//				locInfo.put("", value)
			}
		}
	}

	@Override
	public void reportSubscriberContextStaticInfo(SubscriberStaticInfoResponse arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportSubscriberModificationData(SubscriptionInformationModificationRes arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportTerminalLocation(TerminalLocationData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportTerminalLocationNotification(TerminalLocationData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportTerminalLocationNotificationEnd(NetFuncException arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportTerminalStatus(TerminalStatusData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportTerminalStatusNotification(TerminalStatusData arg0, NetFuncException arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reportTerminalStatusNotificationEnd(NetFuncException arg0) {
		// TODO Auto-generated method stub
		
	}

}
