package io.qsrlabs.nexus.tas.utils;

import java.util.Map;

import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.SipParameterSet;

public class JsonSCIFResponseLegAction extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseLegAction.class.getName());
	CallLeg leg;
	
	
	public JsonSCIFResponseLegAction(Call aCall, Map<String, Object> rawMessage, JsonSession session, CallLeg leg){
		Long diff = getTimeDiff(rawMessage);
		_log.debug("Processing response for leg :{}, timediff:{}, rawMessage:",leg,diff,rawMessage);
		this.call=aCall;
		this.leg = leg;
		
		SipParameterSet sipPset = call.getParameterSet(SipParameterSet.class);
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		action = (Map<String,Object>) rawMessage.get(ACTION);
	}
	
	@Override
	public void execute() {
		_log.debug("Executing for leg:{} ... :{}",leg, action);
		if( action!=null){
			//get action type
			Integer type = (Integer) action.get(ACTION_TYPE);
			String legAction = (String) action.get(LEG_ACTION);
			if(type!=null && type ==3 && legAction!=null) {
				//this is leg action
				processLegAction(action, leg);
			} else {
				processAction(action);
			}
		}		
		_log.debug("Executing ... - DONE");
	}

}
