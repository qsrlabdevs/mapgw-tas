package io.qsrlabs.nexus.tas.cap.servicelogic;

import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import io.qsrlabs.nexus.tas.cap.businesslogic.Parameters;
import io.qsrlabs.nexus.tas.utils.JsonSCIFRequest;
import io.qsrlabs.nexus.tas.utils.JsonSCIFResponse;
import io.qsrlabs.nexus.tas.utils.RedisPool;
import io.qsrlabs.nexus.tas.utils.RedisProcessorThread;
import com.hp.opencall.ngin.scif.resources.IvrCallLeg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.resources.IvrCallLeg.IvrUser;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class CapIvrUser implements IvrUser {
	private static final Logger _log = LoggerFactory.getLogger(CapIvrUser.class.getName());

	private WeakReference<JsonSCIFRequest> jsonCall ;
	private WeakReference<JsonSession> jsonSession;
	private WeakReference<IvrCallLeg> ivrLeg;


	public CapIvrUser(JsonSCIFRequest jsonCall, JsonSession jsonSession, IvrCallLeg leg){
		_log.debug("Initialize CapIvrUser leg:{}",leg);
		this.jsonCall =  new WeakReference<>( jsonCall );
		this.jsonSession = new WeakReference<>( jsonSession );
		this.ivrLeg = new WeakReference<>(leg);
	}

	@Override
	public void collectComplete(Cause cause, String collectedDigits) {
		_log.debug("Collect complete cause:{}",cause);
		try {
			Map<String, Object> eventData = new HashMap<>();
			eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.COLLECTCOMPLETE);
			eventData.put(JsonSCIFRequest.EventId, jsonCall.get().getEventId());
			eventData.put(JsonSCIFResponse.CALL, jsonCall.get().getCall());
			eventData.put(JsonSCIFResponse.SESSION, jsonSession.get());
			eventData.put(JsonSCIFResponse.LEG, this.ivrLeg.get());

			Map<String, Object> rawMessage = jsonCall.get().getJsonCollectComplete(cause,collectedDigits);
			RedisProcessorThread.parkEvent(jsonCall.get().getCallId(), eventData);
			boolean status =  RedisPool.get().sendToQueue(rawMessage, Parameters.get().getQueueEgress());
		} finally {
			jsonCall.get().incrementEventId();
		}


	}

	@Override
	public void playComplete(Cause cause) {
		_log.debug("Play complete cause:{}",cause);
		try {
			Map<String, Object> eventData = new HashMap<>();
			eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.PLAYCOMPLETE);
			eventData.put(JsonSCIFRequest.EventId, jsonCall.get().getEventId());
			eventData.put(JsonSCIFResponse.CALL, jsonCall.get().getCall());
			eventData.put(JsonSCIFResponse.SESSION, jsonSession.get());
			eventData.put(JsonSCIFResponse.LEG, this.ivrLeg.get());

			Map<String, Object> rawMessage = jsonCall.get().getJsonPlayComplete(cause);
			RedisProcessorThread.parkEvent(jsonCall.get().getCallId(), eventData);
			boolean status =  RedisPool.get().sendToQueue(rawMessage, Parameters.get().getQueueEgress());
		} finally {
			jsonCall.get().incrementEventId();
		}
	}

}
