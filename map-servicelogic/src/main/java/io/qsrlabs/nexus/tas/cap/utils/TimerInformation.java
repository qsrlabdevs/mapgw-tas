package io.qsrlabs.nexus.tas.cap.utils;

import java.io.Serializable;

public class TimerInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public enum TIMER_TYPE{
		NO_ANSWER,
		MAX_CALL_DURATION,
		ASYNCHRONOUS_CALL		
	}
	TIMER_TYPE	timerType;
	public TIMER_TYPE getTimerType() {
		return timerType;
	}
	public void setTimerType(TIMER_TYPE timerType) {
		this.timerType = timerType;
	}

}
