package io.qsrlabs.nexus.tas.utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.*;
import com.hp.opencall.ngin.scif.parameters.CsCallParameterSet.RedirectingReason;
import com.hp.opencall.ngin.scif.parameters.CsCallParameterSet.TcapEndMode;
import com.hp.opencall.ngin.scif.resources.*;
import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import io.qsrlabs.nexus.tas.cap.servicelogic.CapCallUser;
import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;

/**
 * 
 * This message is received from business logic and it contains
 * - header rules subscribed to
 * - header rules vars
 * - events subscribed to
 * - header rules ruleset
 * - timers
 * - capabilities
 * - ringing tone
 * - action
 * 		- type: 0-Abort, 1-forwardCall, 2-MRF
 * 			ABORT:
 * 				- ErrorCode
 * 				- Cause
 * 			ForwardCall
 * 				- uri
 * 				- legName
 * 			MRF:
 * 				- earlyMedia : true/false
 * 				- legName
 */
public abstract class JsonSCIFResponse {
	
	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponse.class.getName());

	public enum ActionType{
		AbortCall,
		ForwardCall,
		MRFCall,
		ContinueCall,
		LeaveCall
	}

	Map<String, Object> rawMessage;
	Call call;
	
	String rawJson=null;
	List<String> headerRules;
	Map<String,Object> events;
	Map<String,Object> headerRulesVars;
	String headerRuleSelected=null;
	List<Map<String, Object>> timers;
	List<String> capabilities;
	List<Map<String,Object>> ringingTones;
	Map<String,Object> action;
	
	
	public static final String HEADER_RULES="headerrules";
	public static final String HEADER_VARS="headerrulevar";
	public static final String HEADER_SELECT="headerrulesselect";
	public static final String EVENTS="events";
	public static final String TIMERS="timers";
	public static final String TIMEOUT = "timeout";
	public static final String TIMER_NAME = "name";
	public static final String UI_ANN = "anno_name";
	public static final String ANN_TYPE = "anno_type";
	public static final String ACTION = "action";	
	public static final String ACTION_TYPE="type";
	public static final String ERRORCODE="errorcode";
	public static final String CAUSE="cause";
	public static final String URIs="uri";
	public static final String CALLING_NUMBER="callingParty";
	public static final String EARLYMEDIA="earlymedia";
	
	
	public static final String SuccessReponseActionAccept="accept";
	public static final String SuccessReponseActionForward="forward";
	public static final String SuccessReponseActionReject="reject";

	private static final String LEG_NAME = "legname";

	public static final String LEG_ACTION = "legaction";
	public static final String LEG_RELEASE = "release";
	public static final String LEG_PLAYPROMPT = "playPrompt";
	public static final String LEG_PROMPTCOLLECT = "promptCollect";
	public static final String LEG_REJECTMEDIAOPERATION = "rejectMediaOperation";
	public static final String MEDIA_LANGUAGE = "lang";
	public static final String MEDIA_REPEAT ="repeat";
	public static final String MEDIA_DURATION ="duration";
	public static final String MEDIA_DELAY="delay";

	public static final String MEDIA_NAME = "media";


	public static final String EVENTNAME = "eventname";

	public static final String CALL = "call";

	public static final String SESSION = "session";

	public static final String LEG = "leg";

	public static final String POLLEVENT = "pollEvent";
	public static final String FIRE_FORGET="fireAndForget";
	public static final String CALL_USER = "callUser";

	
	private long startTime=Calendar.getInstance().getTimeInMillis();
	
	protected Long getTimeDiff(Map<String, Object> rawMessage){
		Long startTime = (Long)rawMessage.get(JsonSCIFRequest.StartTime);
		Long stopTime = Calendar.getInstance().getTimeInMillis();
		Long diff=new Long(-1);
		if( startTime!=null && stopTime!=null){
			diff = stopTime-startTime;
		}
		return diff;
	}


	public JsonSCIFResponse(){
		
	}
	
	public JsonSCIFResponse(Call aCall, Map<String, Object> rawMessage, JsonSession session, CallLeg leg){
		Long diff = getTimeDiff(rawMessage);
		_log.debug("Processing response for leg :{}, timediff:{}, rawMessage:",leg,diff,rawMessage);
		call=aCall;
		SipParameterSet sipPset = call.getParameterSet(SipParameterSet.class);
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		action = (Map<String,Object>) rawMessage.get(ACTION);
		if( action!=null){
			//get action type
			Integer type = (Integer) action.get(ACTION_TYPE);
			String legAction = (String) action.get(LEG_ACTION);
			if(type!=null && type ==3 && legAction!=null) {
				//this is leg action
				processLegAction(action, leg);
			}
		}
	}
	
	
	protected Contact processAction(Map<String,Object> action){
		Contact ret=null;
		Integer type = (Integer)action.get(ACTION_TYPE);
		Integer errorCode = (Integer) action.get(ERRORCODE);
		String cause = (String) action.get(CAUSE);
		Cause ntwkCause = Cause.NONE;
		try {
			if (cause != null)
				ntwkCause = Cause.valueOf(cause);
		} catch (Exception e){
			_log.debug("Exception on set network cause",e);
		}

		Map<String, Object> uri = (Map<String, Object>) action.get(URIs);
		Map<String, Object> genericNumber = (Map<String, Object>) action.get(CALLING_NUMBER);
		Boolean earlyMedia = (Boolean) action.get(EARLYMEDIA);
		if( earlyMedia==null){
			earlyMedia = false;
		}
		String legName = (String) action.get(LEG_NAME);

		//action type can be:
		//0 - abort calls
		//1 - forwardCall
		//2 - connect to announcement
		//3 - continue call


		
		CsCallParameterSet csPset = (CsCallParameterSet) call.getParameterSet(CsCallParameterSet.class);
		CommonParameterSet commPset = (CommonParameterSet) call.getParameterSet(CommonParameterSet.class);
		ChargingParameterSet chargPset = (ChargingParameterSet) call.getParameterSet(ChargingParameterSet.class);
		CsSmsParameterSet csSMS = (CsSmsParameterSet) call.getParameterSet(CsSmsParameterSet.class);
		MapCsSmsCommonParameterSetIf mapSMS = (MapCsSmsCommonParameterSetIf) call.getParameterSet(CommonParameterSet.class);

		_log.debug("cs :{} comm:{} charg:{} sms:{} mapsms:{}",csPset,commPset,chargPset,csSMS,mapSMS);

		if(genericNumber!=null){
			_log.debug("Set generic number:{}",genericNumber);
			Address genericNumberAddr = TelecomUtils.mapToAddr(genericNumber);
			try {
				commPset.setCallingParty(genericNumberAddr);
			} catch (ScifException e) {
				_log.error("Exception:",e);
			}
		}

		//set calling party category
		Integer callingPartyCategory = (Integer)action.get("callingPartyCategory");
		if( callingPartyCategory!=null){
			csPset.setCallingPartysCategory(callingPartyCategory);
		}

		//set duration
		Map<String, Object> duration = (Map<String, Object>) action.get("duration");
		if(duration!=null){
			Integer amount = (Integer) duration.get("amount");
			Integer isFinal = (Integer) duration.get("final");

			if( amount!=null) {
                ChargingUnitsExt[] chargingUnits = new ChargingUnitsExt[1];
                chargingUnits[0] = new ChargingUnitsExt();
                amount = amount * 1000;
                BigInteger bigAmount = BigInteger.valueOf(amount);
                chargingUnits[0].setBigAmount(bigAmount);
                chargingUnits[0].setUnit(ChargingUnits.Unit.TIME_MILLISECONDS);

                if (isFinal != null) {
                    chargingUnits[0].setLastUnits((isFinal == 1));
                }

                _log.debug("Grant units:{}", chargingUnits);
                try {
                    chargPset.setGrantedUnits(chargingUnits);
                } catch (ScifException e) {
                    _log.error("Error on grating time units", e);
                }
            }
		}

		//set FCI in case is provided
		String fci = (String) action.get("fci");
		if( fci!=null){
		    FurnishChargingInfo fciOp = new FurnishChargingInfo();
            byte[] fciBytes = DatatypeConverter.parseHexBinary(fci);
            fciOp.setData(fciBytes);

            _log.debug("Sending fci:{}",fciOp);
            try {
                chargPset.setFurnishChargingInformation(fciOp);
            } catch (ScifException e) {
                _log.error("Exception in fci",e);
            }
        }

        //set calll information request
        List<String> infoRequest = (List<String>) action.get("requestedInformation");
		if( infoRequest!=null){
			_log.debug("Set CIR:{}",infoRequest);
		    List<CsCallParameterSet.RequestedInformationList> cirs = new ArrayList<>();
		    for( String infoReq : infoRequest){
                CsCallParameterSet.RequestedInformationList reqInfo=null;
		        if( infoReq.equalsIgnoreCase("CALL_STOP_TIME")) {
                    reqInfo = CsCallParameterSet.RequestedInformationList.CALL_STOP_TIME;
                } else if (infoReq.equalsIgnoreCase("CALL_CONNECTED_ELAPSED_TIME")) {
                    reqInfo = CsCallParameterSet.RequestedInformationList.CALL_CONNECTED_ELAPSED_TIME;
                }
                if( reqInfo!=null ) {
                    cirs.add(reqInfo);
                }
            }
            if( cirs.size()>0) {
				_log.debug("Sending CIR:{}", cirs);
				csPset.setCallInfoReqList(cirs);
			}
        }

		//set possible CsParamSet values
		Map<String, Object> cs = (Map<String, Object>)action.get("cs");
		if( cs!=null){
			
		}

		if( mapSMS!=null){
			try {

				Map<String, Object> smscAddress = (Map<String, Object>) action.get("smscAddress");
				if (smscAddress != null) {
					Address address = TelecomUtils.mapToAddr(smscAddress);
					_log.debug("Set SMSC address:{}",address);
					mapSMS.setSMSCAddress(address);
				}

				Map<String, Object> originSccpAddress = (Map<String, Object>) action.get("originSccpAddress");
				if( originSccpAddress!=null){
					Address address = TelecomUtils.mapToAddr(originSccpAddress);
					_log.debug("Set OriginSccpAddress address:{}",address);
					mapSMS.setOutboundOriginSccpAddress(address);
				}
			} catch (Exception e){
				_log.error("Exception",e);
			}
		}

		ActionType actType = ActionType.values()[type];

		switch(actType){
			case AbortCall:
				_log.debug("Sending abort call errorCode:{}, cause:{}",errorCode,ntwkCause);
				//this is abort call,
                //send TC_END
				if( errorCode!=null ){
					_log.debug("Set error code:{}",errorCode);
					mapSMS.setErrorCode(errorCode);
				}

				call.abortCall(ntwkCause);
				break;
			case ForwardCall:
				Address address = TelecomUtils.mapToAddr(uri);
				TerminalContact termContact = new TerminalContact(address);
				ret = termContact;
				try {
					call.forwardCall(termContact);
				} catch (ScifException e) {
					_log.error("Exception",e);
				}
				_log.debug("Sending forward call contact:{}",ret);
			break;
			case MRFCall:
				try {
					IvrContact ivrContact = ScifFactory.instance().getResourceContact(
							call,
							IvrContact.class,
							null);
					ivrContact.setLegName("ivr");
					ivrContact.setEarlyMedia(earlyMedia);


					ret = ivrContact;
					call.forwardCall(ret);
				} catch (ScifException e) {
					_log.error("Exception",e);
				}
			
			_log.debug("Sending forward call to IVR contact:{}",ret);
			break;
			case ContinueCall:
				try {
					_log.debug("Continue call");
					call.continueCall();
				} catch (ScifException e) {
					e.printStackTrace();
				}
				break;
			case LeaveCall:
				try {
					_log.debug("Leave call uri:{}",uri);
					TerminalContact leaveCallContact=null;
					Address leaveCallAddr=null;
					if( uri!=null) {
						leaveCallAddr = TelecomUtils.mapToAddr(uri);
						leaveCallContact = new TerminalContact(leaveCallAddr);
					}
					ret = leaveCallContact;

					String scheme = (String)uri.get("scheme");
					if( scheme!=null && !scheme.equalsIgnoreCase("tel") ){
						_log.debug("Set address fwd parameters ...");
						Address.NetworkContext nwkCtx = new Address.NetworkContext(Address.Q713Standard.STANDARD_NAME,
								Address.Q713Standard.Nai.INTERNATIONAL_NUMBER,
								Address.Q713Standard.Npi.E163_E164);

						leaveCallAddr.setParameter(Address.Parameter.NETWORK_CONTEXT, nwkCtx.toString());
						leaveCallAddr.setParameter(Address.Parameter.GT_INDICATOR, Address.GtIndicator.TYPE4);
						leaveCallAddr.setParameter(Address.Parameter.GT_TRANSLATION_TYPE, Address.GtTranslationtype.UNUSED);
					}

					_log.debug("Sending leaveCall to:{}",leaveCallContact.toString());

					call.leaveCall(Cause.DONE, leaveCallContact);
				} catch (ScifException e){
					_log.error("Exception",e);
				}
				break;
		}

		
		return ret;
	}
	
	protected void processLegAction(Map<String, Object> action, CallLeg leg){
		_log.debug("legAction:{} leg:{}",action,leg);
		//is assumed that leg is of type RawMedia
		if( leg instanceof IvrCallLeg){
			IvrCallLeg ivrLegMedia = (IvrCallLeg) leg;
			String legAction = (String) action.get(LEG_ACTION);
			Map<String, Object> legActionMap = (Map<String, Object> )action.get(legAction);
			_log.debug("actionLeg:{} map:{}",legAction,legActionMap);
			if( legActionMap!=null){
				if( legAction.equalsIgnoreCase(LEG_RELEASE)){
					_log.debug("Action - RELEASE leg ...");
					//fetch cause
					String cause = (String)legActionMap.get(CAUSE);
					Cause relCause = Cause.NONE;
					if( cause!=null){
						relCause = Cause.valueOf(cause);
					}
					try {
						_log.debug("releasing leg:{}",ivrLegMedia.toString());
						leg.release(relCause);
					} catch (ScifException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (legAction.equalsIgnoreCase(LEG_PLAYPROMPT)){
					_log.debug("Performing media operation ...");
					String mediaName = (String)legActionMap.get(MEDIA_NAME);
					String language = (String) legActionMap.get(MEDIA_LANGUAGE);
					
					Properties props = new Properties();
					
					String repeat = (String) legActionMap.get(MEDIA_REPEAT);
					if(repeat!=null){
						props.setProperty(MEDIA_REPEAT, repeat);
					}
					String duration = (String) legActionMap.get(MEDIA_DURATION);
					if(duration!=null){
						props.setProperty(MEDIA_DURATION, duration);
					}
					String delay = (String) legActionMap.get(MEDIA_DELAY);
					if(delay!=null){
						props.setProperty(MEDIA_DELAY, delay);
					}
					
							
					
					MediaOperation mediaOperation = new MediaOperation("CamelService/"+mediaName);
					if( language!=null) {
						mediaOperation.setLanguage(language);
					}
					
					if (!props.isEmpty()){
						_log.debug("Set parameters:{}",props);
						mediaOperation.setParameters(props);
					}
					
					try {
						ivrLegMedia.play(mediaOperation);
					} catch(Exception e){
						_log.error("Error playing announcement",e);
					}

				}	else if (legAction.equalsIgnoreCase(LEG_PROMPTCOLLECT)){
					_log.debug("Performing media operation ...");
					String mediaName = (String)legActionMap.get(MEDIA_NAME);
					String language = (String) legActionMap.get(MEDIA_LANGUAGE);

					MediaOperation mediaOperation = new MediaOperation("CamelService/"+mediaName);
					if( language!=null) {
						mediaOperation.setLanguage(language);
					}
					try {
						ivrLegMedia.playCollect(mediaOperation);
					} catch(Exception e){
						_log.error("Error playing prompt announcement",e);
					}

				}
			}
		}
	}
	
	
	public abstract void execute();

	static JsonSCIFResponse getResponse(Map<String, Object> rawResponse, Map<String, Object> eventData){
		_log.debug("getResponse eventData:{} message:{}",eventData, rawResponse);
		JsonSCIFResponse resp=null;
		
		String eventName = (String) rawResponse.get(EVENTNAME);
		Integer eventId = (Integer) rawResponse.get(JsonSCIFRequest.EventId);
		String eventIdUniq=(String) rawResponse.get(JsonSCIFRequest.EVENTIdentifier);

		Call call=null;
		JsonSession session=null;

		CapCallUser callUser = null;

		if( eventData!=null) {
			call = (Call) eventData.get(CALL);
			session = (JsonSession) eventData.get(SESSION);
			callUser = (CapCallUser) eventData.get(JsonSCIFResponse.CALL_USER);
		}

		if( callUser!=null ){
			String queueAffinity=(String )rawResponse.get("queue");
			if( queueAffinity!=null ) {
				callUser.setQueue(queueAffinity);
			}
		}
		
		if( eventName.equalsIgnoreCase(JsonSCIFRequest.CALLSTART)){
			resp = new JsonSCIFResponseCallStart(call, rawResponse, session);
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLPOLL)) {
			ScifEvent pollEvent = (ScifEvent) eventData.get(POLLEVENT);

			resp = new JsonSCIFResponseCallPoll(call, rawResponse, session, pollEvent);

			
		} else if (eventName.equalsIgnoreCase(JsonSCIFRequest.CALLANSWERED) || 
				eventName.equalsIgnoreCase(JsonSCIFRequest.CALLEARLYANSWERED) ||
				eventName.equalsIgnoreCase(JsonSCIFRequest.PLAYCOMPLETE) ||
				eventName.equalsIgnoreCase(JsonSCIFRequest.COLLECTCOMPLETE)){

			//leg actions are related to events: callAnswered, callEarlyAnswered, PlayComplete, CollectComplete

			CallLeg leg = (CallLeg) eventData.get(LEG);
			resp = new JsonSCIFResponseLegAction(call, rawResponse, session, leg);
		}
		
		return resp;
	}

	
	public long getTimeDiff(){
		return Calendar.getInstance().getTimeInMillis()-startTime;
	}
	
	private void csMapToCsParameterSet(Map<String, Object> csJson, CsCallParameterSet csPset){
		if( csJson==null){
			return;
		}
		
		Object value = csJson.get("alertingPattern");
		if( value!=null){
			csPset.setAlertingPattern((Integer) value);
		}
		
		value = csJson.get("applyOCSI");
		if( value!=null){
			csPset.applyOCSI();
		}
		
		value = csJson.get("detectionPointSet");
		if( value!=null){
			try {
				csPset.setDetectionPointSet((String) value);
			} catch (ScifException e) {
				_log.error("detectionPointSet Exception",e);
			}
		}
		
		value = csJson.get("redirectingReason");
		if( value!=null){
			RedirectingReason rr = RedirectingReason.valueOf((String)value);
			csPset.setRedirectingReason(rr);
		}
		
		value = csJson.get("tcapEndMode");
		if( value!=null){
			TcapEndMode te = TcapEndMode.valueOf((String)value);
			try {
				csPset.tcapEndMode(te);
			} catch (ScifException e) {
				_log.error("tcapEndMode Exception",e);
			}
		}
		
	}
}
