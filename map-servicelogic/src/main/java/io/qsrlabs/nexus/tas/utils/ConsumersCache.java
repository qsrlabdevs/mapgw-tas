package io.qsrlabs.nexus.tas.utils;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class ConsumersCache {

    private static ConsumersCache instance=new ConsumersCache();
    Cache<String, Object> consumers = null;

    public static ConsumersCache get(){
        return instance;
    }

    private ConsumersCache(){
        consumers=Caffeine.newBuilder()
                .expireAfterWrite(1, TimeUnit.SECONDS)
                .build();
    }

    public void setConsumer(String consumer){
        consumers.put(consumer, Instant.now().toEpochMilli());
    }
    public Object getConsumer(String consumer){
        return consumers.getIfPresent(consumer);
    }

    public long size() {
        return consumers.estimatedSize();
    }
}
