package io.qsrlabs.nexus.tas.cap.servicelogic;

import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallUser;
import com.hp.opencall.ngin.scif.CallUserFactory;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.imscapi.imslet.ApplicationSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ServiceFactory allows to create {@link ServiceFactory} service call objects.
 */
public class CapUserFactory implements CallUserFactory {
	private static final Logger _log = LoggerFactory.getLogger(CapUserFactory.class.getName());
	/**
	 * A constant holding the name of this service.
	 */
	public static final String NAME = "MapService";
	

	public CapUserFactory() {
		_log.debug("CapUserFactory - create");
	}
	
	public CallUser createUser(Object anExecutionContext, Call aCall) throws ScifException {
		_log.debug("CapUserFactory - createUser call:{} executionContext:{}",aCall, anExecutionContext);

		CallUser ret = null;
		try {
			ApplicationSession anAppSession = (ApplicationSession) anExecutionContext;
			ret= new CapCallUser(aCall, anAppSession);
		} catch (Exception e){
			_log.error("Exception on creating call user",e);
			e.printStackTrace();
		}
		return ret;
	}

	public String getName() {
		return NAME;
	}
}
