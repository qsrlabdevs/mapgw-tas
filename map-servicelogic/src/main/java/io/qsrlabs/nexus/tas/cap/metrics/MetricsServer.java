package io.qsrlabs.nexus.tas.cap.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import io.qsrlabs.nexus.tas.cap.businesslogic.Parameters;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import io.prometheus.client.exporter.common.TextFormat;
import io.prometheus.client.hotspot.DefaultExports;
import io.undertow.Undertow;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


public class MetricsServer extends Thread {
    private static final Logger _log = LoggerFactory.getLogger(MetricsServer.class.getName());

    private MetricRegistry registry=null;

    private int httpMetricsPort=0;

    private static MetricsServer instance=null;


    private MetricsServer() {
        registry = new MetricRegistry();
    }
    public static MetricsServer get() {
        if( instance==null){
            instance=new MetricsServer();

        }
        return instance;
    }

    @Override
    public void run(){
        startMetricsServer(Parameters.get().getHttpPort());
        while(true){
            try {
                _log.debug("Metrics server running ...");
                MetricsServer.get().counterIncrement("tas.appserver."+Parameters.getImsName());
                Thread.sleep(1000);
            } catch (Exception e){
                _log.error("Exception ",e);
            }
        }
    }


    private void startMetricsServer(int httpPort){
        httpMetricsPort = httpPort;
        if(httpPort!=0){
            new DropwizardExports(registry, PrometheusUtils.countersBuilder()).register();
            DefaultExports.initialize();
            initializeMetricsHttpServer(httpMetricsPort);
            _log.warn("embbeded http server for metrics started on port: {}", httpMetricsPort);
        }else{
            _log.warn("metrics are not collected");
        }
    }

    private AtomicBoolean serverShouldContinue = new AtomicBoolean(true);

    public void serverShouldContinue(boolean val){
        serverShouldContinue.set(val);
    }

    public boolean shouldServerContinue(){
        return serverShouldContinue.get();
    }

    public static void metricsHandler(HttpServerExchange exchange) {
        StringWriter writer = new StringWriter();
        try {
            String query = exchange.getQueryString();
            _log.info("starting to build metrics response; query line is={}",query);
            HashSet<String> names = new HashSet<>();
            if (query != null) {
                String[] pairs = query.split("&");
                int nbMetrics = pairs.length;
                for(int i = 0; i < nbMetrics; ++i) {
                    String pair = pairs[i];
                    int idx = pair.indexOf("=");
                    if (idx != -1 && URLDecoder.decode(pair.substring(0, idx), "UTF-8").equals("name[]")) {
                        names.add(URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
                    }
                }
            }
            _log.debug("prometheus filtering :{} metrics:{}",names,CollectorRegistry.defaultRegistry.filteredMetricFamilySamples(names));
            TextFormat.write004(writer, CollectorRegistry.defaultRegistry.filteredMetricFamilySamples(names));
            _log.info("metrics computed; about to send back to scrapper");
        }catch(Exception ex){
            ex.printStackTrace();
            _log.error("Exception",ex);
            writer.write("#error building metrics: " + ex.getMessage() + "\r\n");
        }
        _log.debug("Sending response, :{}",writer.toString());
        exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, "text/plain; version=0.0.4; charset=utf-8");
        exchange.getResponseHeaders().add(Headers.CONTENT_LENGTH, String.valueOf(writer.getBuffer().length()));
        exchange.getResponseSender().send(writer.toString());
    }

    private Undertow metricsHttpServer = null;

    public synchronized  void  initializeMetricsHttpServer(int port)  {
        httpMetricsPort = port;
        try {
            _log.warn("stop http server metrics port on port {}",httpMetricsPort);
            if (metricsHttpServer != null) {
                metricsHttpServer.stop();
                metricsHttpServer = null;
            }
            _log.warn("reset metrics");
            for(String metricName:registry.getNames()){
                registry.remove(metricName);
            }
            CollectorRegistry.defaultRegistry.clear();
            _log.warn("re-register again collectors");
            new DropwizardExports(registry, PrometheusUtils.countersBuilder()).register();
            DefaultExports.initialize();
            _log.warn("restart now the server");
            metricsHttpServer = Undertow.builder().addHttpListener(httpMetricsPort,"0.0.0.0",MetricsServer::metricsHandler).build();
            metricsHttpServer.start();
        }catch(Exception ex){
            _log.error("http metrics server initialization error",ex);
            metricsHttpServer = null;
        }
    }

    public synchronized  void reinitializeMetricsHttpServer(){
        initializeMetricsHttpServer(httpMetricsPort);
    }

    public synchronized void stopMetricsHttpServer(){
        if(metricsHttpServer!=null){
            metricsHttpServer.stop();
            metricsHttpServer = null;
        }
    }

    public synchronized boolean isMetricsHttpServerInitialized(){
        return metricsHttpServer!=null;
    }



    public void counterIncrement(String counter){
        registry.counter(counter).inc();
    }

    public void setHistogram(String histogram, long value){
        registry.histogram(histogram).update(value);
    }


    public void setTimer(String timer, long value){
        registry.timer(timer).update(value, TimeUnit.MILLISECONDS);
    }

    public Timer getTimer(String timer){
        return registry.timer(timer);
    }

}
