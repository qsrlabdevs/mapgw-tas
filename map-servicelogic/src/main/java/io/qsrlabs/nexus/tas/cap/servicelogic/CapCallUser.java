package io.qsrlabs.nexus.tas.cap.servicelogic;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import io.qsrlabs.nexus.tas.cap.metrics.MetricsServer;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.CsCallParameterSet;
import com.hp.opencall.ngin.scif.parameters.CsCommonParameterSubset;
import com.hp.opencall.ngin.scif.resources.IvrCallLeg;
import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import io.qsrlabs.nexus.tas.cap.businesslogic.Parameters;
import io.qsrlabs.nexus.tas.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.imscapi.imslet.ApplicationSession;


public class CapCallUser implements CallUser, MessageInterceptor
 {
	
	public static WeakHashMap<Call, String> calls = new WeakHashMap<>();
	
	private static final Logger _log = LoggerFactory.getLogger(CapCallUser.class.getName());

	private JsonSession jsonSession=null;
	private JsonSCIFRequest jsonCall=null;
	
	/**
	 * The call object and the application session for this callUser
	 */
	private Call call;
	private ApplicationSession appSession;
	private long timeDiff = 0;
//	private RawMediaCallLeg rawMediaLeg =null;
	String ingressQueue ="";
	String sipCallId=null;
	private boolean fireAndFortget=false;
	private String egressQueue="";
	private boolean mapSMSAnswered=false;
	
	public CapCallUser(){

		this.call=null;
		this.appSession=null;
	}
	
	
	
	public CapCallUser(Call aCall, ApplicationSession anAppSession) {
		_log.debug("Creating CapCalluser call:{} session:{}",aCall,anAppSession);
		// get current UTC time in milliseconds since epoch
		long currentTimeMs = Calendar.getInstance().getTimeInMillis();

		//Call jsonCall = new JsonCall(aCall);
		//aCall = JsonCall.swap(jsonCall, (jsonCall=aCall));
		
		// Store the call and the application session in class variables.
		call = aCall;
		appSession = anAppSession;
		appSession.setAttribute("scifCallUser", this);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(anAppSession.getCreationTime());
		timeDiff = currentTimeMs - anAppSession.getCreationTime();
		_log.info("CapCallUser constructor, creation time:{}, " , anAppSession.getCreationTime() );

		jsonSession = new JsonSession(appSession);
		jsonCall = new JsonSCIFRequest(call);
		
	
		ingressQueue = "app_"+jsonCall.getCallId();
		calls.put(aCall, aCall.getId());

		_log.debug("Switching point:{}",call.getParameterSet(CommonParameterSet.class).getSwitchingPoint());



	}

	// ************************ callStart ************************************
	public void callStart(Cause aCause, CallLeg aFirstLeg, Contact aCalledParty) {
		// Log the call Id and the cause for reaching Start state.
		_log.debug("callStart NEW invoked with cause:{}, callleg:{}, contact:{} ", aCause  , aFirstLeg , aCalledParty);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLSTART);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.CALL_USER, this);

		String counter = "tas.map.callStart."+aCause ;
		MetricsServer.get().counterIncrement(counter);
		
		
		Long lstartTime = Calendar.getInstance().getTimeInMillis();
		try{ 
			//prepare JSON message
			Map<String,Object> rawMessage = jsonCall.getJsonCallStart(aCause, aFirstLeg, aCalledParty);
			
			
			ObjectMapper mapper = new ObjectMapper();
			String json=null;
			json = mapper.writeValueAsString(rawMessage);
			
			_log.debug("JSON:{}",json);
			
			if( json!=null){
				//send this to redis queue
				_log.debug("Sending JSON message to redis ...");
				RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
				getValidQueue();
				boolean status = RedisPool.get().sendToQueue(rawMessage, this.egressQueue);
			}
		} catch (Exception e){
			_log.error("Exception:{}",e.getMessage());
		} finally {
			_log.info("Execution time, cause:{}, spent time:{}",aCause, (Calendar.getInstance().getTimeInMillis()-lstartTime));
			jsonCall.incrementEventId();
		}
	}

	// This service does not rely on an early media feature
	public void callEarlyAnswered(CallLeg aNewLeg) {
		String counter = "tas.map.callEarlyAnswered";
		MetricsServer.get().counterIncrement(counter);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLEARLYANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		eventData.put(JsonSCIFResponse.CALL_USER, this);
		
		try{
			// _log.info("Call early-answered");
			Map<String, Object> rawEarlyAnswered = jsonCall.getEarlyMediaAnswered(aNewLeg);
			_log.info(" rawEarlyAnswered:{}",rawEarlyAnswered);

            Map<String, Object> rawAnswered = jsonCall.getEarlyMediaAnswered(aNewLeg);
            _log.info(" rawAnswered:{}",rawAnswered);
            if (aNewLeg instanceof IvrCallLeg){
                IvrCallLeg ivrCallLeg = (IvrCallLeg) aNewLeg;
                _log.debug("MRF leg, set media user:{}",ivrCallLeg.toString());
                ivrCallLeg.setIvrUser(new CapIvrUser(jsonCall, jsonSession, ivrCallLeg));
            }
			

			RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
			getValidQueue();
			boolean status =  RedisPool.get().sendToQueue(rawEarlyAnswered, this.egressQueue);

		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callAnswered(Cause aCause, CallLeg aNewLeg) {
		String counter = "tas.map.callAnswered."+aCause;
		MetricsServer.get().counterIncrement(counter);

		return;

//		Map<String, Object> eventData = new HashMap<>();
//		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLANSWERED);
//		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
//		eventData.put(JsonSCIFResponse.CALL, call);
//		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
//		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
//		eventData.put(JsonSCIFResponse.CALL_USER, this);
//
//		try{
//			_log.info("callAnswered cause:{}, leg:{}",aCause, aNewLeg);
//			Map<String, Object> rawAnswered = jsonCall.getAnswered(aCause, aNewLeg);
//			_log.info(" rawAnswered:{}",rawAnswered);
//			if (aNewLeg instanceof IvrCallLeg){
//				IvrCallLeg ivrCallLeg = (IvrCallLeg) aNewLeg;
//				_log.debug("MRF leg, set media user:{}",ivrCallLeg.toString());
//				ivrCallLeg.setIvrUser(new CapIvrUser(jsonCall, jsonSession, ivrCallLeg));
//			}
//
//			//RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
//			//getValidQueue();
//			//boolean status =  RedisPool.get().sendToQueue(rawAnswered, this.egressQueue);
//
//			//this.mapSMSAnswered=true;
//		} finally {
//			jsonCall.incrementEventId();
//		}
	}
		

	public void callPoll(ScifEvent anEvent) {
		String counter = "tas.map.callPoll."+anEvent.getClass().getSimpleName();
		MetricsServer.get().counterIncrement(counter);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLPOLL);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.POLLEVENT, anEvent);
		eventData.put(JsonSCIFResponse.CALL_USER, this);
				
		try{
			_log.info("callPoll event:{}",eventData);
			Map<String, Object> rawPollMessage = jsonCall.getJsonPollEvent(anEvent);
			_log.debug("Poll raw message:{}",rawPollMessage);

			RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);

			getValidQueue();
			boolean status =  RedisPool.get().sendToQueue(rawPollMessage, this.egressQueue);

		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callEnd(Cause aCause) {
		String counter = "tas.map.callEnd."+aCause;
		MetricsServer.get().counterIncrement(counter);

		try{
			_log.info("CallUser callEnd cause:{}", aCause );
			Map<String, Object> rawEnd = jsonCall.getCallEnd(aCause);
			_log.info(" rawEnd:{}",rawEnd);
			getValidQueue();
			if( !this.mapSMSAnswered) {
				getValidQueue();
				boolean status = RedisPool.get().sendToQueue(rawEnd, this.egressQueue);
			}

		} finally {
			jsonCall.incrementEventId();
			
			//remove events from parking slot
			RedisProcessorThread.remove(jsonCall.getCallId());
		}
	}


	public String getCallId() {
		return jsonCall.getCallId();
	}



	public void setCall(Call newCall) {
		this.call= newCall;
	}


	public Call getCall(){
		return this.call;
	}

	 public void setFireAndForget(Boolean fireAndForget) {
		this.fireAndFortget = fireAndForget;
	 }


	 public void setQueue(String value){

		 ConsumersCache.get().setConsumer(value);
		 this.egressQueue =value;


	 }

	 @Override
	 public void messageReceived(Object o, CallLeg callLeg) {
		 InMessageInterceptorExtn msg = (InMessageInterceptorExtn)o;

		 _log.debug("messageReceived opcode:{} pdu:{}",msg.getOperationCode(),msg.getPdu());


	 }

	 @Override
	 public void sendingMessage(Object o, CallLeg callLeg) throws ScifException {

	 }

	 private void getValidQueue(){
		 _log.debug("Existing queue:{}",this.egressQueue);
		 if(!this.egressQueue.equalsIgnoreCase(Parameters.get().getQueueEgress()) &&
				 ConsumersCache.get().getConsumer(this.egressQueue)==null){
			 this.egressQueue=Parameters.get().getQueueEgress();
		 }
		 _log.debug("Valid queue:{}",this.egressQueue);
	 }
 }


