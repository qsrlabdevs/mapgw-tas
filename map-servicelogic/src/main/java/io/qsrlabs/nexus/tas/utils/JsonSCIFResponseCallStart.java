package io.qsrlabs.nexus.tas.utils;

import java.util.Map;

import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;

public class JsonSCIFResponseCallStart extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFResponseCallStart.class.getName());
	
	public JsonSCIFResponseCallStart(Call aCall, Map<String, Object> rawMessage, JsonSession session){
//		super(aCall, rawMessage, session);
		
		Long diff = getTimeDiff(rawMessage);
		call =aCall;
		CommonParameterSet commPset = call.getParameterSet(CommonParameterSet.class);
		
		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);
		
		try {
			if( rawMessage!=null){
				//8. action: forward call
				action = (Map<String,Object>) rawMessage.get(ACTION);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void execute() {
		_log.debug("Executing ... ");
		try{
			Contact contact = processAction(action);
//			if( contact!=null){
//				_log.debug("Forward call to:{}",contact);
//				call.forwardCall(contact);
//			}
		} catch (Exception e){
			e.printStackTrace();
		}
		_log.debug("Executing ... - DONE");
	}
}
