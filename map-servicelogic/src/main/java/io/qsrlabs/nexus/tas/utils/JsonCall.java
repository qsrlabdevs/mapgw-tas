package io.qsrlabs.nexus.tas.utils;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.hp.opencall.ngin.cdrlog.Cdr;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.ParameterSet;
import com.hp.opencall.ngin.scif.ScifException;

/**
 * 
 * Proxy Call class, used to catch events like service forward and alter parameters 
 *
 */
public class JsonCall implements Call {
	private static final Logger _log = LoggerFactory.getLogger(JsonCall.class.getName());
	
	
	private Call originalCall;
	
	public JsonCall(Call origCall){
		originalCall = origCall;
	}
	
	
	@Override
	public void abortCall(Cause cause) {
		originalCall.abortCall(cause);
	}

	@Override
	public void continueCall() throws ScifException {
		originalCall.continueCall();
	}

	@Override
	public void forwardCall(Contact contact) throws ScifException {
		_log.debug("forwardCall contact:{}",contact.toString());
		originalCall.forwardCall(contact);
	}

	@Override
	public Cdr getCdr() {
		return originalCall.getCdr();
	}

	@Override
	public String getId() {
		return originalCall.getId();
	}

	@Override
	public CallLeg[] getLegs() {
		return originalCall.getLegs();
	}

	@Override
	public String getNetworkTechnology() {
		return originalCall.getNetworkTechnology();
	}

	@Override
	public <T extends ParameterSet> T getParameterSet(Class<T> arg0) {
		return originalCall.getParameterSet(arg0);
	}

	@Override
	public State getState() {
		return originalCall.getState();
	}

	@Override
	public void joinCall(Call arg0) throws ScifException {
		originalCall.joinCall(arg0);
	}

	@Override
	public void leaveCall(Cause arg0, Contact arg1) throws ScifException {
		originalCall.leaveCall(arg0, arg1);
	}

	@Override
	public void makeCall(Address arg0, Contact arg1) throws ScifException {
		originalCall.makeCall(arg0, arg1);
	}

	@Override
	public void makeCall(Address arg0, Contact arg1, Contact arg2, Properties arg3) throws ScifException {
		originalCall.makeCall(arg0, arg1,arg2,arg3);
	}

	@Override
	public void splitCall(Call arg0) throws ScifException {
		originalCall.splitCall(arg0);
	}

	@Override
	public void transferCall(TransferMode arg0, CallLeg arg1, Contact arg2) throws ScifException {
		originalCall.transferCall(arg0, arg1, arg2);
	}

	
	public static Call swap(Call call1, Call call2){
		return call1;
	}
}
