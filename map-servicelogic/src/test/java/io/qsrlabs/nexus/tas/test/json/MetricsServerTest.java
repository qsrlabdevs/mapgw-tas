package io.qsrlabs.nexus.tas.test.json;

import com.hp.opencall.ngin.scif.Address;
import io.qsrlabs.nexus.tas.cap.metrics.MetricsServer;
import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class MetricsServerTest {

    @Test
    public void testServer(){
        MetricsServer.get().start();
        while(true){
            //MetricsServer.get().counterIncrement("tas.in.callAttempt");
            try {
                Thread.sleep(1000);
            } catch (Exception e){

            }
        }
    }

    @Test
    public void testSCCPGt(){
        String adressStr="gtpcssn:33660800000.1452.8";
        Address address = new Address(adressStr);
        System.out.println("Address:"+address.toString());

        Map<String,Object> addrJson = new HashMap<>();
        addrJson.put("scheme","gtpcssn");
        addrJson.put("user","33660800000.1452.8");

        Address address1 = TelecomUtils.mapToAddr(addrJson);
        System.out.println("Address1:"+address1.toString());

        String add = "33660800000.8";
        String addrs2[] = add.split("\\.");
        System.out.println(addrs2[0]);
    }
}
