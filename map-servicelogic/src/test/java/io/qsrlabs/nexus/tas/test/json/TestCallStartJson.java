package io.qsrlabs.nexus.tas.test.json;

import java.util.HashMap;
import java.util.Map;

import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import org.junit.Test;

import io.qsrlabs.nexus.tas.utils.RedisPool;
import com.hp.opencall.ngin.scif.Address;

public class TestCallStartJson {

	public void init(){
		RedisPool.get().initialize("redis://192.168.182.131:26379/mymaster", "10", 1000);
	}

	@Test
	public void testTrim(){
		String myVar = "CALL-1 - AppSess-1";
		System.out.println(myVar.replaceAll("\\s",""));
	}

//	@Test
public void testCallContinue(){
		init();
		Map<String, Object> jsonCallStart = new HashMap<>();
		
		jsonCallStart.put("type",1);



		Address destAddr = new Address();
		destAddr.setScheme("tel");
		destAddr.setUser("407222111000");
		destAddr.setParameter(Address.Parameter.NETWORK_CONTEXT , Address.TS24008Standard.encode(Address.TS24008Standard.Nai.INTERNATIONAL_NUMBER, Address.TS24008Standard.Npi.E164));

		Map<String, Object> destAddrJson = TelecomUtils.addressToJson(destAddr);
		jsonCallStart.put("uri",destAddrJson);
		
		destAddr = new Address();
		destAddr.setUser("40722000000");
		destAddr.setScheme("tel");
		destAddr.setParameter(Address.Parameter.NUMBER_QUALIFIER_IND, Address.NumberQualifier.ADDITIONAL_CALLING_PARTY_NUMBER);
		destAddr.setParameter(Address.Parameter.SCREENING , Address.Screening.USER_PROVIDED_VERIFIED_AND_PASSED);
		destAddr.setParameter(Address.Parameter.PRESENTATION , Address.Presentation.ALLOWED);
		destAddr.setParameter(Address.Parameter.NETWORK_CONTEXT , Address.TS24008Standard.encode(Address.TS24008Standard.Nai.INTERNATIONAL_NUMBER, Address.TS24008Standard.Npi.E164));
		
		Map<String, Object> cgPaAddrJson = TelecomUtils.addressToJson(destAddr);
		jsonCallStart.put("calling_number",cgPaAddrJson);
		jsonCallStart.put("callid", "callid");
		jsonCallStart.put("callid", "callid");
		
		
		
		Address newCgPA = TelecomUtils.mapToAddr(cgPaAddrJson);
		
//		RedisPool.get().sendToQueue(jsonCallStart, "test");
		
//		Map<String, Object> action = RedisPool.get().getFromQueue("test");
//		
//		if( action!=null){
//			JsonSCIFResponse resp = new JsonSCIFResponseCallStart(null, action, null);
//		}
		
	}
}
