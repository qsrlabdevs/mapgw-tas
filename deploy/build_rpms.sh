#!/usr/bin/env bash

#export BASE_BUILD_DIR=$PWD/../

#REQUIRED variables in environment when launching this script:

#BASE_BUILD_DIR - it is the root of project. On my laptop is /Users/me/qsrlabs/nexus-gui as an example
#IMAGE - name of the docker image you use; it will be inserted in rpm summary attribute to track the build process
#DEPS_KEY - name of the key to our maven (deps.co) repository
#DEPS_SECRET - secret to our maven (deps.co) repository

which fpm
if [ $? -ne 0 ]; then
    echo "this machine/station does not have fpm package manager in PATH or fpm package manager is not installed"
    echo "please install https://github.com/jordansissel/fpm package manager"
    exit 1
fi

do_rpm=1
which rpmbuild
if [ $? -ne 0 ]; then
    echo "this machine does not have rpmbuild installed (from rpm-build rpm package); we cannot create RPMs therefore!"
    echo "but we continue... maybe you want only the ansible tar files"
    do_rpm=0
fi


cd ${BASE_BUILD_DIR}; mvn clean; mvn package

#rpm version; this is what customer see, not uberjar version.
VERSION_RPM_GW=1.0
VERSION_RPM_GUI_HANDLERS=1.0
VERSION_RPM_RTE_HANDLERS=1.0
#which uberjar you are putting in; version from maven or lein; it might not be the same with rpm version
VERSION_UBERJAR=2.0.4
#RELEASE (iteration) of the same software version
ITERATION_GW=1
ITERATION_RTE=1
ITERATION_GUI=1
#OS should be the OS type, the one you obtain with uname -s; for macos is Darwin, for example.
OS=Linux
#OS distribution; it can be el7 for RHEL 7, el6 for RHEL 6, fc30 for latest fedora, centos for CentOS, etc. it is a list that's why it is between paranthesis
DISTRIBUTION=(el6 el7)

rm -rf ${BASE_BUILD_DIR}/roles/nexus-tas-mapservice/files/*.ear

cp map-servicelogic-ear/target/map-servicelogic*.ear roles/nexus-tas-mapservice/files/


#you should not change here; it adds epoch custom tag and the reference to git project used to build this.
EPOCH=`date +%s`
COMMIT=`git log --pretty=format:'%H' -n 1`
PROJECT=`git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p'`
BRANCH=`git rev-parse --abbrev-ref HEAD`
#IMAGE variable is taken from the environment where this script runs; it is meant to keep the docker image name used to generate this

if [ ${do_rpm} -eq 1 ]; then
    for dist in "${DISTRIBUTION[@]}"
    do

        fpm -s dir \
            -t rpm \
            --verbose  \
            -C ${BASE_BUILD_DIR} \
            -n nexus-sip_rte \
            -v ${VERSION_RPM_GW} \
            --iteration ${ITERATION_GW} \
            --directories /opt/hpe/quasar/nexus-sip  \
            --license "QSR License" \
            --vendor "Quasar Software Research" \
            --category "nexus/sip" \
            --url "http://www.qsrlab.io/" \
            --rpm-summary "Generated based on: ${IMAGE}/${PROJECT}/${BRANCH}/${COMMIT}" \
            --description "this provides the real time engine handlers for SIP" \
            -a all \
            -m "quasar@qsrlab.com" \
            --rpm-dist ${dist} \
            --rpm-tag "Epoch: ${EPOCH}" \
            --rpm-os ${OS} \
            --rpm-user ocvas \
            --rpm-group ocvas \
            --rpm-changelog ${BASE_BUILD_DIR}/changelog_sipservicelogic_scif.md



        fpm -s dir \
            -t rpm \
            --verbose  \
            -C ${BASE_BUILD_DIR} \
            -n nexus-sip-gui \
            -v ${VERSION_RPM_GUI_HANDLERS} \
            --iteration ${ITERATION_GUI} \
            --license "QSR License" \
            --vendor "Quasar Software Research" \
            --category "nexus/gui" \
            --url "http://www.qsrlab.io/" \
            --rpm-summary "Generated based on: ${IMAGE}/${PROJECT}/${BRANCH}/${COMMIT}" \
            --description "this provides the TCAP shapes/handlers in GUI for working with TCAP calls" \
            -a all \
            -m "quasar@qsrlab.com" \
            --rpm-dist ${dist} \
            --rpm-tag "Epoch: ${EPOCH}" \
            --rpm-os ${OS} \
            --rpm-user ocvas \
            --rpm-group ocvas \
            --rpm-changelog ${BASE_BUILD_DIR}/changelog_mapservicelogic_gui.md


    done
fi

fpm -n ansible_roles-${PROJECT}-${BRANCH}-${COMMIT} -t tar -s dir roles/

