service_component create --name "CamelService" --application-type "CamelService"
service_component assign --name "CamelService" --hostname "occpdev" --instance-id "1"


service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v NgIn.Srv.CAMEL.CALL_PROVIDER_TECHNO=CS
service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v NgIn.Srv.CAMEL.CALL_PROVIDER_NAME=CSNL-TCAP-PROVIDER-CAP
service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v CALL_PROVIDER_TECHNO=CS
service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v NgIn.Net.Cs.PROVIDER_NAME=CSNL-TCAP-PROVIDER-CAP
service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v redis.url=redis://192.168.56.102:26379/mymaster
service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v redis.poolsize=10
service_component add_tunable -N "CamelService" -t CoamServiceCfg.CONTEXT_PARAMS -v network_events=camel_events

tcap_provider create --name "CSNL-TCAP-PROVIDER-CAP" --service-component "CamelService" --stack-name "SS7_Stack_1" --tcap-flavour "ITU93" --sccp-flavour "ITU93" --ssn "146" --pc "12392" --gt-tt "UNKNOWN" --gt-nai "INTERNATIONAL_NUMBER" --gt-npi "E163_E164" --gt-digits "491750101401"
tcap_provider create --name "CSNL-TCAP-PROVIDER-CAP" --service-component "CamelService" --stack-name "SS7_Stack_1" --tcap-flavour "ITU93" --sccp-flavour "ITU93" --ssn "146" --pc "10" --gt-tt "UNKNOWN" --gt-nai "INTERNATIONAL_NUMBER" --gt-npi "E163_E164"
tcap_provider assign --name "CSNL-TCAP-PROVIDER-CAP" --app-id "1" --hostname "occpdev" --instance-id "1"

telco_op create -N H3G
country create -N AUSTRIA -C 43
telco_net create -o H3G -c AUSTRIA --name H3G

switching_point create --operator H3G --country AUSTRIA --name "MSC1" --digits "436990071302" --nai "UNKNOWN" --npi "UNKNOWN" --tcap ITU88 --sccp "ITU" --protocol "CAMEL3" --pc "10" --ssn "100"
media_server create --operator H3G --country AUSTRIA --name "IVR_RELAY_MSC1" --digits "436990071302" --nai "UNKNOWN" --npi "UNKNOWN" --tcap ITU93 --switching-point "msc1" --technology "cs:relay"   --type "IVR"     --type "ANNOUNCER"   --weight "0" --tag ocmp