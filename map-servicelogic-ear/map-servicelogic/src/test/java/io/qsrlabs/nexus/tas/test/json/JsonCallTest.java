package io.qsrlabs.nexus.tas.test.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;

import io.qsrlabs.nexus.tas.utils.JsonSCIFResponse;
import com.hp.opencall.ngin.cdrlog.Cdr;
import com.hp.opencall.ngin.scif.Address;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.CallUser;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.InformationalEvent;
import com.hp.opencall.ngin.scif.MessageInterceptor;
import com.hp.opencall.ngin.scif.ParameterSet;
import com.hp.opencall.ngin.scif.ScifEvent;
import com.hp.opencall.ngin.scif.ScifException;
import com.hp.opencall.ngin.scif.TerminalCallLeg;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;


public class JsonCallTest {

	
	
	public JsonCallTest(){
		ArrayList	 aa;
	}
	
	
	@Test
	public void testJsonCall(){
	}
	
	class TestCall implements Call {
		CommonParameterSet commPset;
		
		public TestCall(){
			commPset = new TestCommonParameterSet();
		}
		
		@Override
		public void abortCall(Cause aCause) {
			// TODO Auto-generated method stub

			
		}

		@Override
		public void continueCall() throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void forwardCall(Contact aForwardContact) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Cdr getCdr() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getId() {
			// TODO Auto-generated method stub
			return "1234";
		}

		@Override
		public CallLeg[] getLegs() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getNetworkTechnology() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public <T extends ParameterSet> T getParameterSet(Class<T> aParameterSet) {
			// TODO Auto-generated method stub
			
			if (aParameterSet.getName()==CommonParameterSet.class.getName()) {
				return (T) commPset;
			}
			
			return null;
			
		}

		@Override
		public State getState() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void joinCall(Call aJoinee) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void leaveCall(Cause aCause, Contact aForwardContact) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void makeCall(Address aBehalf, Contact aFirstContact) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void makeCall(Address aBehalf, Contact aFirstContact, Contact aSecondContact, Properties aCallOptions)
				throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void splitCall(Call aNewCall) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void transferCall(TransferMode aMode, CallLeg aClosedLeg, Contact aDestination) throws ScifException {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class CapCallUserTest implements CallUser {

		private TestCall testCall;
		
		public CapCallUserTest(){
			try {
				
				testCall = new TestCall();
		
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		@Override
		public void callAnswered(Cause aCause, CallLeg aNewLeg) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callEarlyAnswered(CallLeg aNewLeg) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callEnd(Cause aCause) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callPoll(ScifEvent anEvent) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void callStart(Cause aCause, CallLeg aFirstLeg, Contact aCalledParty) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	public class TestTerminalCallLeg implements TerminalCallLeg{

		String name="TestTerminalCallLeg";
		Address leg =null;
		
		public TestTerminalCallLeg(){
			leg = new Address();
			leg.setUser("+40723111000");
			leg.setScheme(Address.Scheme.SIP);
			leg.setServer("domain.com");
		}
		
		@Override
		public Address getLegAddress() {
			// TODO Auto-generated method stub
			
			return leg;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return name;
		}

		@Override
		public boolean isEarlyMedia() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void release(Cause arg0) throws ScifException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setName(String arg0) {
			// TODO Auto-generated method stub
			name = arg0;
			
		}

		@Override
		public String getLegId() {
			// TODO Auto-generated method stub
			return null;
		}		
	}
	
	class TestCommonParameterSet implements CommonParameterSet {
		@Override
		public SwitchingPoint getSwitchingPoint() {
			return null;
		}

		@Override
		public void setSwitchingPoint(SwitchingPoint switchingPoint) throws ScifException {

		}

		@Override
		public NetworkCallType getNetworkCallType() {
			return null;
		}

		@Override
		public MediaType getMediaType() {
			return null;
		}

		@Override
		public void setServiceCallType(ServiceCallType serviceCallType) {

		}

		@Override
		public <T extends ServiceCallType> T getServiceCallType(Class<T> aClass) throws ScifException {
			return null;
		}

		@Override
		public <T extends Enum<T>> void setEnumServiceCallType(T t) {

		}

		@Override
		public <T extends Enum<T>> T getEnumServiceCallType(Class<T> aClass) throws ScifException {
			return null;
		}

		@Override
		public String getServiceKey() {
			return null;
		}

		@Override
		public long getCallAttempTime() {
			return 0;
		}

		@Override
		public long getCallConnectedTime() {
			return 0;
		}

		@Override
		public long getCallReleaseTime() {
			return 0;
		}

		@Override
		public void setConnectionTimeout(int i) {

		}

		@Override
		public int getConnectionTimeout() {
			return 0;
		}

		@Override
		public void setActivityTestInterval(int i) throws ScifException {

		}

		@Override
		public void setCallingParty(Address address) throws ScifException {

		}

		@Override
		public Address getRedirectingParty() {
			return null;
		}

		@Override
		public void setRedirectingParty(Address address) throws ScifException {

		}

		@Override
		public int getErrorCode() {
			return 0;
		}

		@Override
		public void setErrorCode(int i) {

		}

		@Override
		public void pollRinging(boolean b) throws ScifException {

		}

		@Override
		public int getNetworkTimeZoneOffSet() {
			return 0;
		}

		@Override
		public <T extends InformationalEvent> void subscribePollEvent(Class<T> aClass, Object... objects) {

		}

		@Override
		public <T extends InformationalEvent> void unsubscribePollEvent(Class<T> anEvent) {

		}

		@Override
		public void setRootCdr(Cdr cdr) throws ScifException {

		}

		@Override
		public void deleteCdr() throws ScifException {

		}

		@Override
		public void setServiceKey(String s) throws ScifException {

		}

		@Override
		public void setMessageInterceptor(MessageInterceptor messageInterceptor) {

		}
	}
	
	private Map<String, Object> prepareJsonResponse(){
		Map<String, Object> rawMessage=new HashMap<>();
		List<String> headerRules = new ArrayList<>();
		headerRules.add("rule1");
		headerRules.add("rule2");
		rawMessage.put(JsonSCIFResponse.HEADER_RULES,headerRules);
		
		
		Map<String, Object> events = new HashMap<>();
		events.put("SuccessResponsePollEvent", null);
		events.put("RawContentPollEvent", "test/test");
		events.put("InfoPollEvent", null);
		
		rawMessage.put(JsonSCIFResponse.EVENTS,events);
		
		Map<String,Object> headerVars = new HashMap<>();
		headerVars.put("histinfo", "mama");
		rawMessage.put(JsonSCIFResponse.HEADER_VARS,headerVars);
		
		rawMessage.put(JsonSCIFResponse.HEADER_SELECT,"none");	

		List<Map<String, Object>> timers = new ArrayList<>(); 
		Map<String, Object> timer1 = new HashMap<>();
		timer1.put(JsonSCIFResponse.TIMEOUT, 1000);
		timer1.put(JsonSCIFResponse.TIMER_NAME, "testTimer");
		timers.add(timer1);
		//rawMessage.put(JsonSCIFResponse.TIMERS,timers);

		Map<String,Object> action = new HashMap<>();
		action.put(JsonSCIFResponse.ACTION_TYPE, 1);
		action.put(JsonSCIFResponse.URIs, "tel:+40723111000");
		
		rawMessage.put(JsonSCIFResponse.ACTION,action);
		
		return rawMessage;
	}
	
}
