package io.qsrlabs.nexus.tas.test.json;

import org.junit.Test;

public class TestSwap {

	class TestClass {
		public TestClass ref;
		public String property;
	}
	
	public TestClass swap(TestClass c1, TestClass c2){		
		return c1;
	}
	
	@Test
	public void testSwap(){
		TestClass inst1, inst2;
		inst1 = new TestClass();
		inst2= new TestClass();
		inst1.property="value1";
		
		inst2.property="value2";
		inst2.ref=inst1;
		
		System.out.println("inst1: ["+inst1.property+" ref:"+inst1.ref+"] inst2: ["+ inst2.property+" ref:"+inst2.ref+"]");
		
		inst1=swap(inst2, (inst2=inst1));
		
		System.out.println("inst1: ["+inst1.property+" ref:"+inst1.ref+"] inst2: ["+ inst2.property+" ref:"+inst2.ref+"]");
		
	}
}
