package io.qsrlabs.nexus.tas.utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.qsrlabs.nexus.tas.cap.metrics.MetricsServer;
import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import io.qsrlabs.nexus.tas.cap.utils.TimerInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.timer.Timer;

/**
 * 
 * This class is used to fetch message from redis queue and issue specific commands
 *
 */
public class RedisProcessorThread extends Thread {

	private static final Logger _log = LoggerFactory.getLogger(RedisProcessorThread.class.getName());
	static Map<String, Object> parkedEvents = new ConcurrentHashMap<>();
	
	public static void parkEvent(String callId, Map<String, Object> event){
//		List<Map<String, Object>> existingEvents = (List<Map<String, Object>> ) parkedEvents.get(callId);
//		if( existingEvents==null){
//			existingEvents = new ArrayList<>();
//			parkedEvents.put(callId, existingEvents);
//		}

        List<Map<String, Object>> existingEvents = (List<Map<String, Object>> ) parkedEvents.computeIfAbsent(callId, k -> new ArrayList<>());
//        if( existingEvents==null){
//            existingEvents = new ArrayList<>();
//            parkedEvents.put(callId, existingEvents);
//        }
		
		existingEvents.add(event);
	}
	
	private String ingressQueue;
	
	public RedisProcessorThread(String ingressQueue){
		_log.debug("Instantiate new processor thread:{}",ingressQueue);
		this.ingressQueue=ingressQueue;
	}
	
	@Override
	public void run(){
		while(true){
			try {
				Map<String, Object> rawResponse = RedisPool.get().getFromQueue(ingressQueue);
				if (RedisPool.get()==null ||
						RedisPool.get().getDB()==null ||
						RedisPool.get().getDB().isClosed()) {
					_log.error("Redis server is not available, stop");
					Thread.sleep(1000);
					return;
				}
				_log.debug("GOT JSON:{}",rawResponse);
				if (rawResponse != null) {
					//fetch callid
					String callId = (String) rawResponse.get(JsonSCIFRequest.CallID);
					if( callId==null){
						continue;
					}
					//
					Integer eventId = (Integer) rawResponse.get("id");
					String eventName = (String) rawResponse.get("eventname");

					String counter = "tas.cap.resp."+eventName;
					MetricsServer.get().counterIncrement(counter);

					//get execution time
					Long now = Instant.now().toEpochMilli();
					Long eventTime = (Long) rawResponse.get("eventtime");
					if( eventTime!=null ){
						long diff = now - eventTime;
						counter = "tas.cap.time."+eventName;
						MetricsServer.get().setHistogram(counter,diff);

						_log.debug("Execution time session:{} time: {}",callId,diff);
					}



					if( eventName.equalsIgnoreCase("makeCall")){
						_log.debug("Running make call for:{}",rawResponse);
						JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse,null);
						resp.execute();
						continue;
					}
					Object parkedEvent = parkedEvents.get(callId);
					_log.debug("Parked event:{}", parkedEvent);
					//fetch event from parked events,
					//search for event id
					if (parkedEvent != null) {
						List<Map<String, Object>> events = (List<Map<String, Object>>) parkedEvent;
						int idx = 0;
						boolean found = false;
						for (Map<String, Object> event : events) {
							if (((Integer) event.get("id")) == eventId) {
								//found event
								_log.debug("Found event:{}", event);
								JsonSCIFResponse resp = JsonSCIFResponse.getResponse(rawResponse, event);
								Call call = (Call) event.get(JsonSCIFResponse.CALL);
								JsonSession session = (JsonSession) event.get(JsonSCIFResponse.SESSION);

								//create fake timer which fires imediately
								JsonSCIFTimerListener timerListener = new JsonSCIFTimerListener(call, resp);
								_log.debug("Arming timer ...");
								Timer timer = TelecomUtils.createTimer(timerListener, 0, session.getApplicationSession(), TimerInformation.TIMER_TYPE.ASYNCHRONOUS_CALL);


//							resp.execute();
								found = true;
								//remove item from list
								break;
							}
							idx++;
						}
						if (found) {
							events.remove(idx);
						}
					} else {
						if( eventName.equalsIgnoreCase("makeCall")){

						}
					}
				}
			} catch (Exception e){
				_log.error("Exception",e);
			}
		}
	}

	public static void remove(String sipCallId) {
		_log.debug("Remove event:{}, parkedEvents size:{}",sipCallId,parkedEvents.size());
		parkedEvents.remove(sipCallId);
	}
}
