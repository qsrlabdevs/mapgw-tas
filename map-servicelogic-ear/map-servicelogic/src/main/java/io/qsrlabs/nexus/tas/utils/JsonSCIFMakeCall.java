package io.qsrlabs.nexus.tas.utils;

import io.qsrlabs.nexus.tas.cap.servicelogic.CapCallUser;
import io.qsrlabs.nexus.tas.cap.servicelogic.CapServiceImslet;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class JsonSCIFMakeCall extends JsonSCIFResponse {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFMakeCall.class.getName());
	private CommonParameterSet commPset=null;
	private CapCallUser callUser=null;
	private Map<String,Object> rawMessage=null;


	public JsonSCIFMakeCall(Map<String, Object> rawMessage){
		_log.debug("Processing make call action ...");
		Long diff = getTimeDiff(rawMessage);
		this.rawMessage = rawMessage;

		//fetch queue
		String queue = (String)rawMessage.get("ingressQueue");

		callUser  = CapServiceImslet.createCall();
		call = callUser.getCall();
		callUser.setQueue(queue);


		Boolean fireAndForget = (Boolean) rawMessage.get(JsonSCIFResponse.FIRE_FORGET);
		if( fireAndForget!=null && callUser!=null){
			callUser.setFireAndForget(fireAndForget);
		}

		commPset = call.getParameterSet(CommonParameterSet.class);

		_log.debug("Incoming timediff:{} rawMessage:{}",diff,rawMessage);

		try {
			if( rawMessage!=null){
				_log.debug("Starting to set CEC and Header rules");

				action = (Map<String,Object>) rawMessage.get(ACTION);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			_log.error("Exception",e);
		}

	}
	
	@Override
	public void execute() {
		_log.debug("executing ...");
		//create new call object

		//fetch destination address
		try {

			callUser.makeCall(rawMessage);

		} catch(Exception e){
			_log.error("Exception",e);
		}

		_log.debug("executing ... - DONE");
	}

}
