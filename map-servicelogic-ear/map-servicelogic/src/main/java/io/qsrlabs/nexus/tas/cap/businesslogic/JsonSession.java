package io.qsrlabs.nexus.tas.cap.businesslogic;

import java.util.ArrayList;
import java.util.List;

import com.hp.opencall.imscapi.imslet.ApplicationSession;

/**
 * 
 * This class holds session related parameters as instructed by business logic application
 *
 */
public class JsonSession  {

	//stores list of header rules variables, make then available to business logic application
	List<String> headerRules;
	ApplicationSession appSession;
	
	public JsonSession(ApplicationSession session){
		headerRules = new ArrayList<>();
		appSession = session;
	}
	
	public List<String> getHeaderRules(){
		return headerRules;
	}
	
	public ApplicationSession getApplicationSession(){
		return appSession;
	}
}
