package io.qsrlabs.nexus.tas.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import com.hp.opencall.ngin.scif.events.CallInfoPollEvent;
import com.hp.opencall.ngin.scif.events.ChargingPollEvent;
import com.hp.opencall.ngin.scif.parameters.*;
import io.qsrlabs.nexus.tas.cap.businesslogic.Parameters;
import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.imscapi.imslet.ApplicationSession;
import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.scif.CallLeg;
import com.hp.opencall.ngin.scif.Cause;
import com.hp.opencall.ngin.scif.Contact;
import com.hp.opencall.ngin.scif.ScifEvent;
import com.hp.opencall.ngin.scif.TerminalContact;

/**
 * 
 * This class is used to transfer data from Call to JsonCall 
 * It has following structure:
 * - callid - specify callid fetched from Call-ID from incoming INVITE
 * - as - specify application server
 * - eventtime - specify event time 
 * - event:
 * 		- this is mapped to scif callStart, callPoll, callEnd, callAnswered
 * 		- name: callStart/callPoll/callEnd/callAnswered
 * 		- callStart:
 * 				- cause: - specify cause received
 * 				- leg: specify leg
 * 				- contact: specify contact
 * 		- callPoll:
 * 				- event
 * 		- callEnd:
 * 				- cause
 * 		- callAnswered:
 * 				- cause
 * 				- leg
 * - call
 * 		- legs
 * 			- capabilities
 * 		- state
 * - SIP:
 * 		- SipParameterSet
 * 			- headerRulesVariables
 * 			- errorCode
 * 			
 * 		- message
 * 			- method
 * 		
 */
public class JsonSCIFRequest {

	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFRequest.class.getName());
	
	private Call call;
	private CallLeg initialLeg;
	public static final String CallID="callid";
	public static final String Queue="queue";
	public static final String ApplicationServer="as";
	public static final String EventTime="eventtime";
	public static final String StartTime="starttime";
	public static final String StopTime="stoptime";
	public static final String Message="message";
	public static final String CallLegs="legs";
	public static final String LEG="leg";
	public static final String CallState="state";
	public static final String Call="call";
	public static final String Event="event";

	public static final String EventName="name";
	public static final String CS_CAUSE="cause";
	public static final String CS_LEG="leg";
	public static final String CS_CONTACT="contact";
	public static final String Name="name";
	public static final String Address="address";
	public static final String Type="type";

	public static final String EventId = "id";
	public static final String EVENTIdentifier = "event-id";

	public static final String CALLSTART = "callStart";


	public static final String CALLPOLL = "callPoll";

	public static final String CALLANSWERED = "callAnswered";

	public static final String CALLEARLYANSWERED = "callEarlyAnswered";

	public static final String PLAYCOMPLETE = "playComplete";
	public static final String COLLECTCOMPLETE = "collectComplete";
	public static final String MAKECALL = "makeCall";

	String callId=null;
	
	int eventId=0;
	long startTimeMilis;

	public JsonSCIFRequest(Call aCall ){
		call = aCall;		
		//set call id

		//prepare unique call id
		callId =  Parameters.getImsName() + "-" +Calendar.getInstance().getTimeInMillis() + "-" + call.getId().replaceAll("\\s","");
		_log.debug("CallID:{}",callId);

		startTimeMilis=Calendar.getInstance().getTimeInMillis();
	}
	
	public final String getCallId(){
		return callId;
	}

	private void setCallGenericFields(Map<String, Object> callContainer){
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, callId);
		callContainer.put(Queue, Parameters.getImsName());

		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put(StartTime, startTimeMilis);

		//4. add call related fields like legs and state
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				Map<String,Object> jsonLeg = new HashMap<>();
				jsonLeg.put(Name, leg.getName());
				jsonLeg.put(Address, TelecomUtils.addressToJson(leg.getLegAddress()));
				legs.add(jsonLeg);
			}
			callMap.put(CallLegs, legs);
		}

		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);


	}

	public Map<String, Object> getJsonCallStart(Cause cause, CallLeg pleg,  Contact contact){		
		Map<String, Object> callContainer = new HashMap<>();
		if( initialLeg==null){
			initialLeg=pleg;
		}
		
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, callId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());

		String eventName="callStart."+cause.toString();
		try {
			CommonParameterSet commonParameterSet = (CommonParameterSet) call.getParameterSet(CommonParameterSet.class);
			//eventName = eventName+"." + commonParameterSet.getMediaType().toString() + "." + commonParameterSet.getNetworkCallType().toString();
		} catch (Exception e){

		}

		callContainer.put("event-type","camel");
		callContainer.put("event-name",eventName);


		//4. call container
		Map<String, Object> callMap = new HashMap<>();
		if( call.getLegs()!=null){
			List<Map<String, Object>> legs = new ArrayList<>();
			for( CallLeg leg : call.getLegs()){
				Map<String,Object> jsonLeg = new HashMap<>();
				jsonLeg.put(Name, leg.getName());
				jsonLeg.put(Address, TelecomUtils.addressToJson(leg.getLegAddress()));
				legs.add(jsonLeg);
			}
			callMap.put(CallLegs, legs);
		}
		
		
		callMap.put(CallState, call.getState());
		callContainer.put(Call, callMap);
		
		//4.1 add event 
		Map<String, Object> event = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, JsonSCIFRequest.CALLSTART);
		event.put(EventId, eventId);
		
		Map<String,Object> callStart = new HashMap<>();
		
		_log.debug("Preparing event callStart with cause:{}",cause);
		callStart.put(JsonSCIFRequest.CS_CAUSE, cause);
		if( pleg!=null)
			callStart.put(JsonSCIFRequest.CS_LEG, TelecomUtils.addressToJson(pleg.getLegAddress()));
		if( contact!=null){
			if( contact instanceof TerminalContact){
				TerminalContact termContact = (TerminalContact) contact;
				callStart.put(JsonSCIFRequest.CS_CONTACT, TelecomUtils.addressToJson( termContact.getAddress() ));
			} else {
				if( contact instanceof TerminalContact){
					callStart.put(JsonSCIFRequest.CS_CONTACT, TelecomUtils.addressToJson(((TerminalContact)contact).getAddress()));
				}
				
			}
		}
		
		Map<String,Object> camelContainer = new HashMap<>();
		event.put("camel", camelContainer);
		//get data from CsParameterSet
		
		csCallParameterSetToJson(camelContainer);

		csSMSParameterSetToJson(camelContainer);

		commonParameterSetToJson(camelContainer);
		
		chargingParameterSetToJson(camelContainer);
		
		event.put(CALLSTART,callStart);
		_log.debug("Prepared callStart:{}",callStart);
		
		_log.debug("sending event:{}",event);
		callContainer.put(Event, event);
		
		
		_log.debug("CallContainer :{}",callContainer);
		
		//in case cause is !=NONE, it means leg has been disconnected and check status like call duration etc
		
		
		
		
		return callContainer;
	}

	private void csCallParameterSetToJson(Map<String, Object> camelContainer){
		try{
			CsCallParameterSet csParamSet = call.getParameterSet(CsCallParameterSet.class);

			if( csParamSet!=null){
				if( csParamSet.getAdditionalCallingPartyNumber()!=null){
					camelContainer.put("addCgPa", TelecomUtils.addressToJson(csParamSet.getAdditionalCallingPartyNumber()));
				}
	
				if( csParamSet.getMscAddress()!=null){
					camelContainer.put("mscAddress", TelecomUtils.addressToJson(csParamSet.getMscAddress()));
				}
				if( csParamSet.getVlrAddress()!=null){
					camelContainer.put("vlrAddress", TelecomUtils.addressToJson(csParamSet.getVlrAddress()));
				}
				if( csParamSet.getLocationNumber()!=null){
					camelContainer.put("locationNumber", TelecomUtils.addressToJson(csParamSet.getLocationNumber()));
				}
				if( csParamSet.getLocationInformation()!=null){
					camelContainer.put("locationInformation", csParamSet.getLocationInformation());
				}
				if( csParamSet.getForwardingDestinationNumber()!=null){
					camelContainer.put("forwardingNumber", TelecomUtils.addressToJson(csParamSet.getForwardingDestinationNumber()));
				}
				if( csParamSet.getOriginalCalledPartyID()!=null){
					camelContainer.put("origCalledPartyID", TelecomUtils.addressToJson(csParamSet.getOriginalCalledPartyID()));
				}
				if( csParamSet.getSccpCalledPartyAddress()!=null){
					camelContainer.put("sccpCdPa", TelecomUtils.addressToJson(csParamSet.getSccpCalledPartyAddress()));
				}
				if( csParamSet.getSccpCallingPartyAddress()!=null){
					camelContainer.put("sccpCgPa", TelecomUtils.addressToJson(csParamSet.getSccpCallingPartyAddress()));
				}


	
	
				camelContainer.put("gsmFwdPending", csParamSet.getGsmFwdPending());
				if( csParamSet.getAlertingPattern()!=null){
					camelContainer.put("alertingPattern", csParamSet.getAlertingPattern());
				}
				camelContainer.put("bearerServiceCode", csParamSet.getBearerServiceCode());
				camelContainer.put("cgPaCategory", csParamSet.getCallingPartysCategory());
				camelContainer.put("callReferenceNumber", csParamSet.getCallReferenceNumber());
				camelContainer.put("eventTypeBCSM", csParamSet.getEventTypeBCSM());
				camelContainer.put("highLayerCharacteristics", csParamSet.getHighLayerCharacteristics());
				camelContainer.put("networkProtocol", csParamSet.getNetworkProtocol().toString());
				if(csParamSet.getRedirectingReason()!=null)
					camelContainer.put("redirectReason", csParamSet.getRedirectingReason().toString());
				
				Map<String, Object> csJson = new HashMap<>();
				camelContainer.put("cs", csJson);
				if( csParamSet.getAlertingPattern()!=null){
					csJson.put("alerintPattern", csParamSet.getAlertingPattern());
				}
				if( csParamSet.getConnectedCallingNumber()!=null){
					csJson.put("connectedCallingNumber", TelecomUtils.addressToJson(csParamSet.getConnectedCallingNumber()));
				}
				if( csParamSet.getConnectedNumber()!=null){
					csJson.put("connectedNumber", TelecomUtils.addressToJson(csParamSet.getConnectedNumber()));
				}
				if( csParamSet.getConnectedRedirectingNumber()!=null){
					csJson.put("connectedRedirectingNumber", TelecomUtils.addressToJson(csParamSet.getConnectedRedirectingNumber()));
				}

				csJson.put("disconnectedFlag", csParamSet.getDisconnectFlag());
				
				if( csParamSet.getExtnBearerServiceCode()!=null ){
					csJson.put("extnBearerServiceCode", csParamSet.getExtnBearerServiceCode());
				}
				if( csParamSet.getExtnTeleServiceCode()!=null){
					csJson.put("extnTeleServiceCode", csParamSet.getExtnTeleServiceCode());
				}
				if( csParamSet.getIMSI()!=null){
					csJson.put("imsi", csParamSet.getIMSI());
				}
				if( csParamSet.getLastACTime()!=0){
					csJson.put("lastACTTime", csParamSet.getLastACTime());
				}
				
				csJson.put("lastNetworkEvent", csParamSet.getLastNetworkEvent());


				
				if( csParamSet.getRawBearerCapability()!=null){
					csJson.put("rawBearerCapability", DatatypeConverter.printHexBinary(csParamSet.getRawBearerCapability()));
				}
				if( csParamSet.getRawRedirectionInformation()!=null ){
					csJson.put("rawRedirectionInformation", DatatypeConverter.printHexBinary(csParamSet.getRawRedirectionInformation()));
				}
				if( csParamSet.getRawReleaseCallCause()!=null ) {
					csJson.put("rawReleaseCallCause", DatatypeConverter.printHexBinary(csParamSet.getRawReleaseCallCause()));
				}
				//TODO - this is not yet supported
//				if( csParamSet.getSendChargingInformation()!=null ){
//					csJson.put("sendChargingInformation", DatatypeConverter.printHexBinary(csParamSet.getSendChargingInformation()));
//				}
				csJson.put("tcapEndMode", csParamSet.getTcapEndMode());
				

				
			}
		} catch(Exception e){
			_log.error("Exception",e);
		}
	}
	
	private void chargingParameterSetToJson(Map<String,Object> camelContainer){
		try{
			ChargingParameterSet chargingPset = (ChargingParameterSet) call.getParameterSet(ChargingParameterSet.class);
			if( chargingPset!=null){
				Map<String, Object> chargingPSetJson = new HashMap<>();
				camelContainer.put("charging", chargingPSetJson);
				if( chargingPset.getUsedUnits()!=null){
					List<Map<String, Object>> usedUnitsJson = new ArrayList<>();
					chargingPSetJson.put("usedUnits",usedUnitsJson);
					
					for( ChargingUnits cg:chargingPset.getUsedUnits() ){
						Map<String, Object> unit = new HashMap<>();
						unit.put("amount" , cg.getAmount());
						unit.put("direction" , cg.getDirection());
						unit.put("reportingReason" , cg.getReportingReason());
						unit.put("tariffChangeUsage" , cg.getTariffChangeUsage());
						unit.put("tariffTimeChange" , cg.getTariffTimeChange());
						unit.put("unit" , cg.getUnit());
						usedUnitsJson.add(unit);
					}

				}			
			}
		} catch (Exception e){
			_log.error("Exception",e);
		}
	}
	
	private void commonParameterSetToJson(Map<String,Object> camelContainer){
		try{
			CommonParameterSet commonPset = (CommonParameterSet) call.getParameterSet(CommonParameterSet.class);
			if( commonPset!=null){
				Map<String, Object> commonJson = new HashMap<>();
				camelContainer.put("common", commonJson);
				
				commonJson.put("callAttemptTime", commonPset.getCallAttempTime());
				commonJson.put("callConnectedTime", commonPset.getCallConnectedTime());
				commonJson.put("callReleaseTime", commonPset.getCallReleaseTime());
				commonJson.put("connectionTimeout", commonPset.getConnectionTimeout());
				commonJson.put("mediaType", commonPset.getMediaType());
				commonJson.put("networkCallType", commonPset.getNetworkCallType());
				commonJson.put("serviceKey", commonPset.getServiceKey());
				if( commonPset.getRedirectingParty()!=null){
					commonJson.put("redirectingParty", TelecomUtils.addressToJson(commonPset.getRedirectingParty()));
				}
				
				commonJson.put("networkTimeZoneOffSet", commonPset.getNetworkTimeZoneOffSet());
				
				
			}
		} catch (Exception e){
			_log.error("Exception",e);
		}
	}

	private void csSMSParameterSetToJson(Map<String,Object> camelContainer){
		try{
			CsSmsParameterSet smsParameterSet = (CsSmsParameterSet) call.getParameterSet(CsSmsParameterSet.class);
			if( smsParameterSet!=null){
				Map<String, Object> smsJson = new HashMap<>();
				camelContainer.put("sms", smsJson);

				smsJson.put("eventType",smsParameterSet.getEventTypeSMS());
				smsJson.put("messageType",smsParameterSet.getMessageType());

				if( smsParameterSet.getConnectedNumber()!=null )
					smsJson.put("connectedNumber",TelecomUtils.addressToJson(smsParameterSet.getConnectedNumber()));
				smsJson.put("imsi",smsParameterSet.getIMSI());
				if( smsParameterSet.getConnectedCallingNumber()!=null )
					smsJson.put("connectedCallingNumber",TelecomUtils.addressToJson(smsParameterSet.getConnectedCallingNumber()));
				if( smsParameterSet.getSgsnAddress()!=null)
					smsJson.put("sgsnAddress",TelecomUtils.addressToJson(smsParameterSet.getSgsnAddress()));
				if( smsParameterSet.getMscAddress()!=null)
					smsJson.put("mscAddress",TelecomUtils.addressToJson(smsParameterSet.getMscAddress()));
				if( smsParameterSet.getVlrAddress()!=null)
					smsJson.put("vlrAddress",TelecomUtils.addressToJson(smsParameterSet.getVlrAddress()));

				if( smsParameterSet.getConnectedRedirectingNumber()!=null)
					smsJson.put("connectedRedirectingNumber",TelecomUtils.addressToJson(smsParameterSet.getConnectedRedirectingNumber()));
				if( smsParameterSet.getGmscAddress()!=null)
					smsJson.put("gmscAddress",TelecomUtils.addressToJson(smsParameterSet.getGmscAddress()));

				if( smsParameterSet.getSccpCalledPartyAddress()!=null)
					smsJson.put("sccpCalledPartyAddress",TelecomUtils.addressToJson(smsParameterSet.getSccpCalledPartyAddress()));
				if( smsParameterSet.getSccpCallingPartyAddress()!=null)
					smsJson.put("sccpCallingPartyAddress",TelecomUtils.addressToJson(smsParameterSet.getSccpCallingPartyAddress()));

				smsJson.put("lastNetworkEvent",smsParameterSet.getLastNetworkEvent());
				smsJson.put("networkProtocol",smsParameterSet.getNetworkProtocol());
				if( smsParameterSet.getLocationInformation()!=null )
					smsJson.put("locationInformation",smsParameterSet.getLocationInformation());
			}
		} catch (Exception e){
			_log.error("Exception",e);
		}
	}
	
	
	public Map<String, Object> getJsonPollEvent(ScifEvent pollEvent) {
		Map<String, Object> callContainer = new HashMap<>();

		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, callId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());

		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put("event-type", "camel");
		callContainer.put("event-name", "callPoll." + pollEvent.getClass().getSimpleName());


		Map<String, Object> event = new HashMap<>();
		//4. set event
		callContainer.put(Event, event);
		Map<String, Object> callPoll = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, CALLPOLL);
		event.put(JsonSCIFRequest.EventId, eventId);
		event.put(CALLPOLL, callPoll);

		_log.debug("PollEvent :{}", pollEvent.getClass().getCanonicalName());

		Map<String, Object> camelContainer = new HashMap<>();
		event.put("camel", camelContainer);
		//get data from CsParameterSet
		csCallParameterSetToJson(camelContainer);
		//get data from CommonParameterSet
		commonParameterSetToJson(camelContainer);
		//get call data from charging parameter set
		chargingParameterSetToJson(camelContainer);


		if (pollEvent instanceof ChargingPollEvent) {
			ChargingPollEvent chargingEvent = (ChargingPollEvent) pollEvent;

			callPoll.put(CS_CAUSE, "ChargingPollEvent");

			if (chargingEvent.getLeg() != null) {
				Map<String, Object> leg = new HashMap<>();
				callPoll.put(LEG, leg);
				Map<String, Object> addr = TelecomUtils.addressToJson(chargingEvent.getLeg().getLegAddress());
				leg.put(Address, addr);
				leg.put(Name, chargingEvent.getLeg().getName());
			}


		} else if (pollEvent instanceof CallInfoPollEvent) {
			CallInfoPollEvent callInfo = (CallInfoPollEvent) pollEvent;
			callPoll.put(CS_CAUSE, "CallInfoPollEvent");

			Map<String,Object> callInfoJson = new HashMap<>();
			if( callInfo.getCallStopTimeValue()!=null) {
				callInfoJson.put("callStopTime", callInfo.getCallStopTimeValue().getTimeInMillis());
			}
			callInfoJson.put("callAttemptElapsedTime",callInfo.getCallAttemptElapsedTimeValue());
			callInfoJson.put("callConnectedElapsedTime",callInfo.getCallConnectedElapsedTimeValue());
			callInfoJson.put("calledPartyNumber",TelecomUtils.addressToJson(callInfo.getCalledPartyNumber()));

			if( callInfo.getReleaseCauseValue()!=null ) {
				callInfoJson.put("releaseCauseValue",DatatypeConverter.printHexBinary(callInfo.getReleaseCauseValue()));
			}

			camelContainer.put("callInfo",callInfoJson);

		} else {
			callPoll.put(CS_CAUSE, pollEvent.getClass().getSimpleName());
		}




		if (pollEvent instanceof ChargingPollEvent) {
			try {
				ChargingParameterSet chargingPset = call.getParameterSet(ChargingParameterSet.class);
				if (chargingPset != null) {
					chargingPset.setChargedUnits(chargingPset.getUsedUnits());
				}
			}catch (Exception e){
				_log.error("Exception",e);
			}
		}


		return callContainer;
	}



	public Map<String, Object> getEarlyMediaAnswered(CallLeg aNewLeg) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, callId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put("event-type","camel");
		callContainer.put("event-name","callEaryMediaAnswered."+aNewLeg.getClass().getSimpleName());


		Map<String, Object> event = new HashMap<>();
		callContainer.put(Event, event);
		
		event.put(JsonSCIFRequest.EventName, CALLEARLYANSWERED);
		event.put(JsonSCIFRequest.EventId, eventId);
		
		Map<String, Object> callEarlyAnswered = new HashMap<>();
		//4. set poll event
		event.put(CALLEARLYANSWERED, callEarlyAnswered);		
		
		callEarlyAnswered.put(CS_LEG, aNewLeg.getLegAddress().toString());
		callEarlyAnswered.put(Type,aNewLeg.getClass().getSimpleName());
		
		return callContainer;
	}



	public Map<String, Object> getAnswered(Cause aCause, CallLeg aNewLeg) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, callId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put("event-type","camel");
		callContainer.put("event-name","callAnswered."+aNewLeg.getClass().getSimpleName());



		Map<String, Object> event = new HashMap<>();
		
		//4. set event
		callContainer.put(Event, event);
		Map<String, Object> callAnswered = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, CALLANSWERED);
		event.put(JsonSCIFRequest.EventId, eventId);
		event.put(CALLANSWERED,callAnswered);
		
		callAnswered.put(CS_LEG, TelecomUtils.addressToJson(aNewLeg.getLegAddress()));
		callAnswered.put(CS_CAUSE, aCause);
		callAnswered.put(Type,aNewLeg.getClass().getSimpleName());

		return callContainer;
	}

	public Map<String, Object> getCallEnd(Cause aCause) {
		Map<String, Object> callContainer = new HashMap<>();
		
		//1. set callID, this is unique identifying the call
		callContainer.put(CallID, callId);
//		callContainer.put(Queue, "app_"+sipCallId);
		callContainer.put(Queue, Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		callContainer.put(ApplicationServer, Parameters.getImsName());
		//3. timestamp of the event
		callContainer.put(EventTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put(StartTime, Calendar.getInstance().getTimeInMillis());
		callContainer.put("event-type","camel");
		callContainer.put("event-name","callEnd");


		Map<String, Object> callEnd = new HashMap<>();
		//4. set poll event
		callContainer.put(Event, callEnd);

		callEnd.put(JsonSCIFRequest.EventId, eventId);
		callEnd.put(JsonSCIFRequest.EventName, "callEnd");
		Map<String, Object> callEndParam = new HashMap<>();
		callEnd.put("callEnd",callEndParam);
		callEndParam.put("cause",aCause);

		
		Map<String,Object> camelContainer = new HashMap<>();
		callEnd.put("camel", camelContainer);
		//get data from CsParameterSet		
		csCallParameterSetToJson(camelContainer);
		//get data from CommonParameterSet
		commonParameterSetToJson(camelContainer);
		//get call data from charging parameter set
		chargingParameterSetToJson(camelContainer);

		
		return callContainer;	
	}
	
	
	public Call getCall(){
		return this.call;
	}

	public ApplicationSession getApplicationSession() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void incrementEventId(){
		eventId++;
	}

	public int getEventId() {
		return eventId;
	}

	public Map<String, Object> getJsonPlayComplete(Cause cause){
		Map<String, Object> jsonPlayComplete = new HashMap<>();
		setCallGenericFields(jsonPlayComplete);
		jsonPlayComplete.put("event-type","camel");
		jsonPlayComplete.put("event-name","playComplete");


		//set playComplete event
		Map<String, Object> playComplete = new HashMap<>();
		playComplete.put(JsonSCIFRequest.CS_CAUSE, cause);

		Map<String, Object> event = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, JsonSCIFRequest.PLAYCOMPLETE);
		event.put(EventId, eventId);

		event.put(PLAYCOMPLETE,playComplete);
		_log.debug("Prepared play complete:{}",playComplete);

		_log.debug("sending event:{}",event);
		jsonPlayComplete.put(Event, event);

		return jsonPlayComplete;
	}

	public Map<String, Object> getJsonCollectComplete(Cause cause, String collectedDigits){
		Map<String, Object> jsonPlayComplete = new HashMap<>();
		setCallGenericFields(jsonPlayComplete);

		jsonPlayComplete.put("event-type","camel");
		jsonPlayComplete.put("event-name","collectComplete");


		//set playComplete event
		Map<String, Object> collectComplete = new HashMap<>();
		collectComplete.put(JsonSCIFRequest.CS_CAUSE, cause);
		collectComplete.put("collectedDigits", collectedDigits);

		Map<String, Object> event = new HashMap<>();
		event.put(JsonSCIFRequest.EventName, JsonSCIFRequest.COLLECTCOMPLETE);
		event.put(EventId, eventId);

		event.put(COLLECTCOMPLETE,collectComplete);
		_log.debug("Prepared collect complete:{}",collectComplete);

		_log.debug("sending event:{}",event);
		jsonPlayComplete.put(Event, event);

		return jsonPlayComplete;
	}
}
