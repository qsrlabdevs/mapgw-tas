package io.qsrlabs.nexus.tas.cap.servicelogic;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

import io.qsrlabs.nexus.tas.cap.metrics.MetricsServer;
import com.hp.opencall.ngin.scif.*;
import com.hp.opencall.ngin.scif.parameters.CommonParameterSet;
import com.hp.opencall.ngin.scif.parameters.CsCallParameterSet;
import com.hp.opencall.ngin.scif.parameters.CsCommonParameterSubset;
import com.hp.opencall.ngin.scif.resources.IvrCallLeg;
import io.qsrlabs.nexus.tas.cap.businesslogic.JsonSession;
import io.qsrlabs.nexus.tas.cap.businesslogic.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.qsrlabs.nexus.tas.cap.utils.TelecomUtils;
import io.qsrlabs.nexus.tas.utils.JsonSCIFRequest;
import io.qsrlabs.nexus.tas.utils.JsonSCIFResponse;
import io.qsrlabs.nexus.tas.utils.RedisPool;
import io.qsrlabs.nexus.tas.utils.RedisProcessorThread;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.opencall.imscapi.imslet.ApplicationSession;


public class CapCallUser implements CallUser
 {
	
	public static WeakHashMap<Call, String> calls = new WeakHashMap<>();
	
	private static final Logger _log = LoggerFactory.getLogger(CapCallUser.class.getName());

	private JsonSession jsonSession=null;
	private JsonSCIFRequest jsonCall=null;
	
	/**
	 * The call object and the application session for this callUser
	 */
	private Call call;
	private ApplicationSession appSession;
	private long timeDiff = 0;
//	private RawMediaCallLeg rawMediaLeg =null;
	String ingressQueue ="";
	String sipCallId=null;
	private boolean fireAndFortget=false;
	
	
	public CapCallUser(){

		this.call=null;
		this.appSession=null;
	}
	
	
	
	public CapCallUser(Call aCall, ApplicationSession anAppSession) {
		_log.debug("Creating CapCalluser call:{} session:{}",aCall,anAppSession);
		// get current UTC time in milliseconds since epoch
		long currentTimeMs = Calendar.getInstance().getTimeInMillis();

		//Call jsonCall = new JsonCall(aCall);
		//aCall = JsonCall.swap(jsonCall, (jsonCall=aCall));
		
		// Store the call and the application session in class variables.
		call = aCall;
		appSession = anAppSession;
		appSession.setAttribute("scifCallUser", this);
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(anAppSession.getCreationTime());
		timeDiff = currentTimeMs - anAppSession.getCreationTime();
		_log.info("CapCallUser constructor, creation time:{}, " , anAppSession.getCreationTime() );

		jsonSession = new JsonSession(appSession);
		jsonCall = new JsonSCIFRequest(call);
		
	
		ingressQueue = "app_"+jsonCall.getCallId();
		calls.put(aCall, aCall.getId());

		_log.debug("Switching point:{}",call.getParameterSet(CommonParameterSet.class).getSwitchingPoint());



	}

	// ************************ callStart ************************************
	public void callStart(Cause aCause, CallLeg aFirstLeg, Contact aCalledParty) {
		// Log the call Id and the cause for reaching Start state.
		_log.debug("callStart NEW invoked with cause:{}, callleg:{}, contact:{} ", aCause  , aFirstLeg , aCalledParty);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLSTART);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);

		String counter = "tas.cap.callStart."+aCause ;
		MetricsServer.get().counterIncrement(counter);
		
		
		Long lstartTime = Calendar.getInstance().getTimeInMillis();
		try{ 
			//prepare JSON message
			Map<String,Object> rawMessage = jsonCall.getJsonCallStart(aCause, aFirstLeg, aCalledParty);
			
			
			ObjectMapper mapper = new ObjectMapper();
			String json=null;
			json = mapper.writeValueAsString(rawMessage);
			
			_log.debug("JSON:{}",json);
			
			if( json!=null){
				//send this to redis queue
				_log.debug("Sending JSON message to redis ...");
				RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
				boolean status = RedisPool.get().sendToQueue(rawMessage, Parameters.get().getQueueEgress());
			}
		} catch (Exception e){
			_log.error("Exception:{}",e.getMessage());
		} finally {
			_log.info("Execution time, cause:{}, spent time:{}",aCause, (Calendar.getInstance().getTimeInMillis()-lstartTime));
			jsonCall.incrementEventId();
		}
	}

	// This service does not rely on an early media feature
	public void callEarlyAnswered(CallLeg aNewLeg) {
		String counter = "tas.cap.callEarlyAnswered";
		MetricsServer.get().counterIncrement(counter);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLEARLYANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		
		try{
			// _log.info("Call early-answered");
			Map<String, Object> rawEarlyAnswered = jsonCall.getEarlyMediaAnswered(aNewLeg);
			_log.info(" rawEarlyAnswered:{}",rawEarlyAnswered);

            Map<String, Object> rawAnswered = jsonCall.getEarlyMediaAnswered(aNewLeg);
            _log.info(" rawAnswered:{}",rawAnswered);
            if (aNewLeg instanceof IvrCallLeg){
                IvrCallLeg ivrCallLeg = (IvrCallLeg) aNewLeg;
                _log.debug("MRF leg, set media user:{}",ivrCallLeg.toString());
                ivrCallLeg.setIvrUser(new CapIvrUser(jsonCall, jsonSession, ivrCallLeg));
            }
			

			RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
			boolean status =  RedisPool.get().sendToQueue(rawEarlyAnswered, Parameters.get().getQueueEgress());

		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callAnswered(Cause aCause, CallLeg aNewLeg) {
		String counter = "tas.cap.callAnswered."+aCause;
		MetricsServer.get().counterIncrement(counter);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLANSWERED);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.LEG, aNewLeg);
		
		try{
			_log.info("callAnswered cause:{}, leg:{}",aCause, aNewLeg);
			Map<String, Object> rawAnswered = jsonCall.getAnswered(aCause, aNewLeg);
			_log.info(" rawAnswered:{}",rawAnswered);
			if (aNewLeg instanceof IvrCallLeg){
				IvrCallLeg ivrCallLeg = (IvrCallLeg) aNewLeg;
				_log.debug("MRF leg, set media user:{}",ivrCallLeg.toString());
				ivrCallLeg.setIvrUser(new CapIvrUser(jsonCall, jsonSession, ivrCallLeg));
			}
			
			//RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);
			boolean status =  RedisPool.get().sendToQueue(rawAnswered, Parameters.get().getQueueEgress());

		} finally {
			jsonCall.incrementEventId();
		}
	}
		

	public void callPoll(ScifEvent anEvent) {
		String counter = "tas.cap.callPoll."+anEvent.getClass().getSimpleName();
		MetricsServer.get().counterIncrement(counter);

		Map<String, Object> eventData = new HashMap<>();
		eventData.put(JsonSCIFResponse.EVENTNAME, JsonSCIFRequest.CALLPOLL);
		eventData.put(JsonSCIFRequest.EventId, jsonCall.getEventId());
		eventData.put(JsonSCIFResponse.CALL, call);
		eventData.put(JsonSCIFResponse.SESSION, jsonSession);
		eventData.put(JsonSCIFResponse.POLLEVENT, anEvent);
				
		try{
			_log.info("callPoll event:{}",eventData);
			Map<String, Object> rawPollMessage = jsonCall.getJsonPollEvent(anEvent);
			_log.debug("Poll raw message:{}",rawPollMessage);

			RedisProcessorThread.parkEvent(jsonCall.getCallId(), eventData);

			boolean status =  RedisPool.get().sendToQueue(rawPollMessage, Parameters.get().getQueueEgress());

		} finally {
			jsonCall.incrementEventId();
		}
	}

	public void callEnd(Cause aCause) {
		String counter = "tas.cap.callEnd."+aCause;
		MetricsServer.get().counterIncrement(counter);

		try{
			_log.info("CallUser callEnd cause:{}", aCause );
			Map<String, Object> rawEnd = jsonCall.getCallEnd(aCause);
			_log.info(" rawEnd:{}",rawEnd);
			boolean status =  RedisPool.get().sendToQueue(rawEnd, Parameters.get().getQueueEgress());
		} finally {
			jsonCall.incrementEventId();
			
			//remove events from parking slot
			RedisProcessorThread.remove(jsonCall.getCallId());
		}
	}


	public String getCallId() {
		return jsonCall.getCallId();
	}



	public void setCall(Call newCall) {
		this.call= newCall;
	}

	
	private void prepareCallConnect(String callid, Map<String, Object> request){
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Map<String, Object> jsonCallStart = new HashMap<>();
		Map<String, Object> action = new HashMap<>();
		action.put("type",1);
		jsonCallStart.put("action", action);

		
		jsonCallStart.put("callid",callid);
		jsonCallStart.put("eventtime",Calendar.getInstance().getTimeInMillis());
		jsonCallStart.put("starttime", request.get("starttime"));
				
		jsonCallStart.put("queue", Parameters.getImsName());
		
		//2. metadata, related to appserver which generated this call
		jsonCallStart.put("as", Parameters.getImsName());
		//3. timestamp of the event
		
		Address destAddr = new Address();
		destAddr.setScheme("tel");
		destAddr.setUser("407222111000");
		destAddr.setParameter(Address.Parameter.NETWORK_CONTEXT , Address.TS24008Standard.encode(Address.TS24008Standard.Nai.INTERNATIONAL_NUMBER, Address.TS24008Standard.Npi.E164));


		Map<String, Object> destAddrJson = TelecomUtils.addressToJson(destAddr);
		action.put("uri",destAddrJson);
		
		destAddr = new Address();
		destAddr.setUser("40722000000");
		destAddr.setScheme("tel");
		destAddr.setParameter(Address.Parameter.NUMBER_QUALIFIER_IND, Address.NumberQualifier.ADDITIONAL_CALLING_PARTY_NUMBER);
		destAddr.setParameter(Address.Parameter.SCREENING , Address.Screening.USER_PROVIDED_VERIFIED_AND_PASSED);
		destAddr.setParameter(Address.Parameter.PRESENTATION , Address.Presentation.ALLOWED);
		destAddr.setParameter(Address.Parameter.NETWORK_CONTEXT , Address.TS24008Standard.encode(Address.TS24008Standard.Nai.INTERNATIONAL_NUMBER, Address.TS24008Standard.Npi.E164));
		
		Map<String, Object> cgPaAddrJson = TelecomUtils.addressToJson(destAddr);
		action.put("calling_number",cgPaAddrJson);
		jsonCallStart.put("id",0);
		jsonCallStart.put("eventname","callStart");

		
		
		RedisPool.get().sendToQueue(jsonCallStart, Parameters.getImsName());

	}

	public Call getCall(){
		return this.call;
	}

	 public void setFireAndForget(Boolean fireAndForget) {
		this.fireAndFortget = fireAndForget;
	 }


	 public void makeCall( Map<String,Object> raw)  {
		 Map<String,Object> makeCall = (Map<String,Object>) raw.get("makeCall");
		 CommonParameterSet cPset = call.getParameterSet(CommonParameterSet.class);
		 CsCallParameterSet csPset = call.getParameterSet(CsCallParameterSet.class);

		 _log.debug("Make call...:{}",makeCall);

		 try {

			if( makeCall.containsKey("serviceKey")){
				call.getParameterSet(CommonParameterSet.class).setServiceKey(makeCall.get("serviceKey").toString());
			}

			csPset.setNetworkProtocol(CsCommonParameterSubset.NetworkProtocol.CAMEL2);
			Map<String,Object> sccpAddress = (Map<String,Object>) makeCall.get("sccpCalledParty");
			Address sccpdestAddress = TelecomUtils.mapToAddr(sccpAddress);

			_log.debug("makeCall scpAddress=" + sccpdestAddress);
			csPset.setSccpCalledPartyAddress(sccpdestAddress);

			Map<String,Object> locationNumber = (Map<String,Object>) makeCall.get("locationNumber");
			Address locationNumberAddr = TelecomUtils.mapToAddr(locationNumber);
			csPset.setLocationNumber(locationNumberAddr);
			if( makeCall.containsKey("callingPartysCategory")) {
				csPset.setCallingPartysCategory((Integer) makeCall.get("callingPartysCategory"));
			}
			if(makeCall.containsKey("highLayerCharacteristics")) {
				csPset.setHighLayerCharacteristics((Integer) makeCall.get("highLayerCharacteristics"));
			}
			if(makeCall.containsKey("eventTypeBcsm")) {
				csPset.setEventTypeBCSM((Integer) makeCall.get("eventTypeBcsm"));
			}
			if(makeCall.containsKey("callReferenceNumber")) {
				csPset.setCallReferenceNumber((String)makeCall.get("callReferenceNumber"));
			}

			if(makeCall.containsKey("rawBearerCapability")) {
				//csPset.setRawBearerCapability((String)makeCall.get("rawBearerCapability"));
			}

			Map<String,Object> mscAddress = (Map<String,Object>)makeCall.get("mscAddress");
			if( mscAddress!=null) {
				Address mscAddr = TelecomUtils.mapToAddr(mscAddress);
				csPset.setMscAddress(mscAddr);
			}

		 } catch (ScifException e) {
			 _log.error("Unable to set parameters", e);

		 }

		 Map<String,Object> callingParty = (Map<String,Object>)makeCall.get("callingParty");
		 Address cgPa=TelecomUtils.mapToAddr(callingParty);

		 Map<String,Object> calledParty = (Map<String,Object>)makeCall.get("calledParty");
		 Address cdPa=TelecomUtils.mapToAddr(calledParty);
		 TerminalContact terminalContact = new TerminalContact(cdPa);

		 try {
			 call.makeCall(cgPa, terminalContact);
		 } catch (ScifException e) {
			 _log.error("Unable to make the Call", e);
		 }
	 }

	 public void setQueue(String value){
		this.ingressQueue=value;
	 }

 }


