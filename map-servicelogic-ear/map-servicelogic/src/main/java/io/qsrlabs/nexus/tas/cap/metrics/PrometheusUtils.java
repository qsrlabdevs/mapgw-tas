package io.qsrlabs.nexus.tas.cap.metrics;

import io.prometheus.client.dropwizard.samplebuilder.CustomMappingSampleBuilder;
import io.prometheus.client.dropwizard.samplebuilder.MapperConfig;
import io.prometheus.client.dropwizard.samplebuilder.SampleBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PrometheusUtils {
    public static SampleBuilder countersBuilder(){
        MapperConfig tasPing = new MapperConfig();
        tasPing.setMatch("tas.appserver.*");
        tasPing.setName("appServer");
        {
            Map<String, String> labels = new HashMap<>();
            tasPing.setLabels(labels);
            labels.put("name","${0}");
        }



        MapperConfig callStart = new MapperConfig();
        callStart.setMatch("tas.cap.callStart.*");
        callStart.setName("callStart");
        {
            Map<String, String> labels = new HashMap<>();
            callStart.setLabels(labels);
            labels.put("cause","${0}");
        }



        MapperConfig callPoll = new MapperConfig();
        callPoll.setMatch("tas.cap.callPoll.*");
        callPoll.setName("callPoll");
        {
            Map<String, String> labels = new HashMap<>();
            callPoll.setLabels(labels);
            labels.put("cause","${0}");
        }



        MapperConfig callAnswered = new MapperConfig();
        {
            callAnswered.setMatch("tas.cap.callAnswered.*");
            callAnswered.setName("callAnswered");
            Map<String, String> labels = new HashMap<>();
            callAnswered.setLabels(labels);
            labels.put("cause","${0}");
        }


        MapperConfig callEarlyAnswered = new MapperConfig();
        callEarlyAnswered.setMatch("tas.cap.callEarlyAnswered");
        callEarlyAnswered.setName("callEarlyAnswered");


        MapperConfig callEnd = new MapperConfig();
        callEnd.setMatch("tas.cap.callEnd.*");
        callEnd.setName("callEnd");
        {
            Map<String, String> labels = new HashMap<>();
            callEnd.setLabels(labels);
            labels.put("cause", "${0}");
        }


        MapperConfig configTimeRes = new MapperConfig();
        configTimeRes.setMatch("tas.cap.time.*");
        configTimeRes.setName("response_time");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            configTimeRes.setLabels(lab);
        }

        MapperConfig tasResponse = new MapperConfig();
        tasResponse.setMatch("tas.cap.resp.*");
        tasResponse.setName("response");
        {
            Map<String, String> lab = new HashMap<>();
            lab.put("type", "${0}");
            tasResponse.setLabels(lab);
        }

        return new CustomMappingSampleBuilder(Arrays.asList(tasPing,callStart,callAnswered,callEarlyAnswered,callPoll,callEnd,configTimeRes,tasResponse));
    }
}
