package io.qsrlabs.nexus.tas.cap.businesslogic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.qsrlabs.nexus.tas.cap.servicelogic.CapServiceImslet;
import com.hp.opencall.ngin.timer.Timer;

/**
 * 
 * This class it is used to periodically fetch emscli parameters
 *
 */
public class Parameters extends Thread {
	private static final Logger _log = LoggerFactory.getLogger(Parameters.class.getName());
	
	private static final String REDIS_URL = "redis.url";
	private static final String REDIS_POOL_SIZE = "redis.poolsize";
	private static final String REDIS_QUEUE_EGRESS = "redis.queue.egress";
	private static final String METRICS_PORT = "metrics.httpport";


	private CapServiceImslet imslet;

	private int parametersRefreshTimer=30000;

	public String getRedisUrl() {
		return redisUrl;
	}

	public void setRedisUrl(String redisUrl) {
		this.redisUrl = redisUrl;
	}

	public String getRedisPoolSize() {
		return redisPoolSize;
	}

	public void setRedisPoolSize(String redisPoolSize) {
		this.redisPoolSize = redisPoolSize;
	}

	private String redisUrl;
	private String redisPoolSize;
	private Integer httpPort=50000;

	public String getQueueEgress() {
		return queueEgress;
	}

	private String queueEgress;


	public Parameters(){

	}
	public Parameters(CapServiceImslet imslet){
		this.imslet=imslet;		
	}
	
	private static Parameters instance=null;

	private static String mImsName;

	
	static public void initializeInstance(CapServiceImslet imslet){
		instance = new Parameters(imslet);
		instance.reloadConfig();
		instance.start();
	}
	
	static public  Parameters get(){
		if( instance==null){
			instance=new Parameters();
		}
		return instance;
	}
	
	private void reloadConfig() {
	       reloadParameters();
	       reloadConfigFiles();
	}
	
	private void reloadConfigFiles() {
	}
	
	private void reloadParameters() {
        if( this.imslet==null){
        	return;
        }

        try {
            this.redisUrl = this.imslet.getImsletContext().getInitParameter(REDIS_URL);
            _log.debug("redisUrl:{}",redisUrl);
        } catch (Exception e) {
            _log.error("Error in loading redisUrl");
        }
        try {
            this.redisPoolSize = this.imslet.getImsletContext().getInitParameter(REDIS_POOL_SIZE);
            _log.debug("redisPoolSize:{}",redisPoolSize);
        } catch (Exception e) {
            _log.error("Error in loading redisPoolSize");
        }
		try {
			this.queueEgress = this.imslet.getImsletContext().getInitParameter(REDIS_QUEUE_EGRESS);
			_log.debug("redisPoolSize:{}",redisPoolSize);
		} catch (Exception e) {
        	this.queueEgress="camel_events";
			_log.error("Error in loading redisPoolSize");
		}

		try {
			this.httpPort = Integer.parseInt(this.imslet.getImsletContext().getInitParameter(METRICS_PORT));
			_log.debug("metrics port:{}",httpPort);
		} catch (Exception e) {
			_log.error("Error in loading ,metrics port");
		}

	}


	public void timeout(Timer timer) {		
        reloadConfig();

		if( timer!=null){
			timer.cancel();
		}		
	}


	@Override
	public String toString() {
		return "Parameters ";
	}


	public int getParametersRefreshTimer() {
		return parametersRefreshTimer;
	}


	public static void setImsName(String imsname) {
		mImsName = imsname; 
	}

	public static String getImsName() {
		if( mImsName==null){
			return "none";
		}
		return mImsName ;
	}

	public Integer getHttpPort() {
		return this.httpPort;
	}

	public void run(){
		while(!imslet.isShouldStop()){
			_log.debug("Reload config parameters");
			reloadConfig();
			
			try {
				_log.debug("Sleeping: "+parametersRefreshTimer + " seconds");
				Thread.sleep(parametersRefreshTimer*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}



	
	
}
