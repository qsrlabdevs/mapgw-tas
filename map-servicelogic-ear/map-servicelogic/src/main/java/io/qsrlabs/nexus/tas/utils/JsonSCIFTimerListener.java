package io.qsrlabs.nexus.tas.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.opencall.ngin.scif.Call;
import com.hp.opencall.ngin.timer.Timer;
import com.hp.opencall.ngin.timer.TimerListener;

public class JsonSCIFTimerListener implements TimerListener{
	private static final Logger _log = LoggerFactory.getLogger(JsonSCIFTimerListener.class.getName());
	
	private Call call;
	private JsonSCIFResponse resp;
	
	
	public JsonSCIFTimerListener(Call aCall){
		call = aCall;
	}

	public JsonSCIFTimerListener(Call aCall, JsonSCIFResponse resp){
		call = aCall;
		this.resp=resp;
	}

	@Override
	public void timeout(Timer timer) {
		_log.debug("Timeout poped up ...");
		//stop timer
		if( timer!=null){
			timer.cancel();
		}
		
		if( resp!=null){
			_log.debug("Added lantency:{}",resp.getTimeDiff());
			resp.execute();
			
		}
		
		/*
		//terminate UAC legs
		for( CallLeg leg : call.getLegs()){
			_log.debug("Leg:{}",leg);
			if( leg.getName().equalsIgnoreCase("UAC")){
				_log.debug("terminate leg:{}, reason DISCONNECTED",leg);
				try {
					leg.release(Cause.DISCONNECTED);
				} catch (ScifException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		*/
		
		//TODO - notify business logic of timer expiration
		//Is not needed because there will come callStart disconnect as response
	}

}
