package io.qsrlabs.nexus.tas.utils;


import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import com.eitg.quasar.nexus.middleware.utils.Commons;
import com.eitg.quasar.nexus.middleware.utils.RedisDatabase;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisSentinelPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;
import redis.clients.util.Pool;

/**
 * 
 * Utility class providing redis pool
 *
 */
public class RedisPool {
	private static final Logger log = LoggerFactory.getLogger(RedisPool.class.getName());
	
//	private JedisSentinelPool queueDb;
	private Pool<Jedis> queueDb;
	private Pool<Jedis> monitorDb;
	
	private static RedisPool instance;
	
	//TODO - create setter for redis sentinels
	private static String redisSentinel="192.168.56.131:26379";
	private static String redisMaster="mymaster";
	private static int getTimeout=10;
	
	
	public static RedisPool get(){
		if( instance == null){
			instance = new RedisPool();
		}
		return instance;
	}
	
	public void initialize() {
		Set<String> sentinels = new TreeSet<>();
        sentinels.add(redisSentinel);
        queueDb = new JedisSentinelPool(redisMaster,sentinels);
        
        
	}
	
	public Pool<Jedis> getDB() {
		return queueDb;
	}
	
	public void initialize(String redisUrl,String sizeOfPools, Integer timeout) {
		queueDb = createPool(redisUrl,sizeOfPools);	
		getTimeout = timeout;
	}

	public synchronized void initialize(String redisUrl) {
		Map<String,Object> gwConfig = (Map<String, Object>) Commons.readConfigJsonFromRedis(redisUrl);
		log.debug("GW Config:{}",gwConfig);

		if( gwConfig==null){
			log.error("Cannot find gw configuration.");
			System.exit(1);
		}

		String name = (String)gwConfig.get("name");

		Map<String,Object> tasGw = (Map<String, Object>) gwConfig.get("TASGw");


		Map<String,Object> queue = (Map<String,Object>)tasGw.get("queue");
		Map<String,Object> monitor = (Map<String,Object>)tasGw.get("monitor");

		queueDb = RedisDatabase.createRedisPool(queue,"queuedb");
		monitorDb = RedisDatabase.createRedisPool(monitor,"monitor");

	}

	private JedisSentinelPool createPool(String redisUrl,String sizeOfPools) {
		log.debug("redis url:{}",redisUrl);
        JedisSentinelPool pool = null;
        if (redisUrl == null) {
            log.error("null redis url while attempting to initialize the pool");
            return null;
        }
        String urlParts[] = redisUrl.split("/");
        if ( urlParts.length < 3) {
            log.error("wrong redis url format; too few parts");
            return null;
        }
        Jedis conn = null;
        try {
            if (!urlParts[0].startsWith("redis")) {
                log.error("Url should start with redis");
                pool = null;
            } else {
                int poolSize = 20;
                try {
                    poolSize = Integer.parseInt(sizeOfPools);
                } catch (Exception ex) {
                    log.error("parameter pool-size is not a number", ex);
                }
                GenericObjectPoolConfig config = new GenericObjectPoolConfig();
                String clusterName = urlParts[urlParts.length - 1];
                String sentinels[] = urlParts[2].split(",");
                config.setMaxTotal(poolSize + 5);
                config.setMinIdle(poolSize);
                config.setMaxIdle(poolSize);
                config.setJmxEnabled(true);
                config.setJmxNameBase(clusterName);
                config.setJmxNamePrefix("nexus");
                config.setTestOnBorrow(false);
                config.setTestOnReturn(false);
                config.setTestOnCreate(true);
                config.setTestOnReturn(false);
                config.setTestWhileIdle(false);
                config.setSoftMinEvictableIdleTimeMillis(500);
                config.setNumTestsPerEvictionRun(poolSize / 2);
                config.setBlockWhenExhausted(false);
                config.setMinEvictableIdleTimeMillis(500);
                config.setTimeBetweenEvictionRunsMillis(60000);
                log.info("attempting to create pool for master {} and sentinels {}", clusterName, sentinels);
                Set<String> setOfSentinels = new TreeSet<>();
                Collections.addAll(setOfSentinels, sentinels);
                pool = new JedisSentinelPool(clusterName, setOfSentinels, config);
                log.trace("Pool is created, we have to validate it by attempting a write operation");
                conn = pool.getResource();
                Random randKeyName = new Random();
                String testKey = Integer.toString(randKeyName.nextInt());
                if (conn.incr(testKey) < 1) {
                    throw new Exception("cannot write into pool");
                }
                conn.del(testKey);
                log.info("OK, pool initialized ok and validated");
            }
        } catch (Exception ex) {
            log.error("cannot create pool:", ex);
            pool = null;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        
        
        return pool;
    }
	
	/**
	 * Method used to fetch message from specific queue
	 * @param queue
	 * @return
	 */
	public Map<String, Object> getFromQueue(String queue){
		if( queueDb==null) {
			return null;
		}
		
		Map<String, Object> ret = null;
		try ( Jedis con = queueDb.getResource()){
			log.debug("get message from queue:{} timeout:{}",queue, getTimeout);
			List<String> messages = con.brpop(getTimeout, queue);
			if( messages!=null) {
				log.debug("message received size:{}", messages.size());
			}
			if( messages!=null && messages.size()>1){
				String msg = messages.get(1);
				log.info("JSON received: {}",msg);
				//2 create Map object from message received
				ret = stringToMap(msg);
			}
		} catch (Exception e) {
			log.error("Exception on getFromQueue:{}",e.toString());
		}
		log.debug("Message out:{}",ret);
		return ret;
	}
	
	public boolean sendToQueue(Map<String, Object> message, String queue){
		if( queueDb==null) {
			return false;
		}
		
		boolean ret=false;
		String json = mapToString(message);
		try ( Jedis con = queueDb.getResource(); Pipeline pipeline = con.pipelined()){
			log.debug("LPUSH {} {}",queue,json);

			Response<Long> status = pipeline.lpush(queue, json);
			pipeline.sync();
			ret = true;
		} catch (Exception e) {
			log.error("Error on sendToQueue:{}",e.toString());
		}
		return ret;
	}
	
	private Map<String, Object> stringToMap(String json){
		Map<String, Object> ret=null;
		ObjectMapper mapper = new ObjectMapper();
    	
		try {
			ret = (HashMap<String, Object>)mapper.readValue(json,HashMap.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	private String mapToString(Map<String,Object> map){
		String ret = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			ret = mapper.writeValueAsString(map);	
			
		} catch (JsonProcessingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}		
		
		return ret;
	}
	
	
	public Map<String,Object> get(String key){
		if( queueDb==null) {
			return null;
		}
		
		Map<String, Object> ret = null;
		try ( Jedis con = queueDb.getResource(); Pipeline pipeline = con.pipelined()){
			log.debug("get message for key:{} timeout:{}",key, getTimeout);
			Response<String> value = pipeline.get(key);
			pipeline.sync();
			
			ret = stringToMap(value.get());
		} catch (Exception e) {
			log.error("Exception on get:{}",e.toString());
		}
		log.debug("Message out:{}",ret);
		return ret;
	}
}
